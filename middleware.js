var jwt = require("jsonwebtoken");
var fs = require("fs-extra");
const userModelName = require("./mongodb/Models/User").model
const mongoose = require("mongoose")
const UserSchema = mongoose.model(userModelName)
const { validationResult } = require('express-validator/check');


ensureToken = function(req, res, next){
    console.log("ensureToken")
    const bearerHeader = req.headers["authorization"];

    if(typeof  bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];

        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403)
    }
}

authTokenConfirmed = function(req, res, next){
    console.log("authTokenConfirmed")

    jwt.verify(req.token, fs.readFileSync("Keys/confirmed.key", "utf8"), function(err, data) {
        if(err){
            console.log("403")
            res.sendStatus(403)
        }else{
            req.tokenData = data;
            next();
        }
    })
}

authTokenNotConfirmed = function(req, res, next){
    console.log("authTokenNotConfirmed")
    jwt.verify(req.token, fs.readFileSync("Keys/notConfirmed.key", "utf8"), function(err, data) {
        console.log(data);
        if(err){
            console.log("403")
            res.sendStatus(403)
        }else{
            req.tokenData = data;
            next();
        }
    })
}

checkIfUserExists = function(req, res, next){
    console.log("checkIfUserExists")

        UserSchema.findById(req.tokenData._id).exec( (err, user)=>{

            if(err){throw err};
            if(user){


                if(user.accountStatus == 1){
                    req.user = user;
                    next();
                }else if(user.accountStatus == 2){
                    res.sendStatus(401);
                }
            }else{
                res.sendStatus(401);
            }
        })
}

fixTable = function(req, res, next){
    console.log("checkIfUserExists")

    console.log(req.tokenData)
    UserSchema.findById(req.tokenData._id).exec( (err, user)=>{

        console.log(err)

        if(err){throw err};

        console.log(user)
        if(user){


            if(user.accountStatus == 1){
                req.user = user;
                next();
            }else if(user.accountStatus == 2){
                res.sendStatus(401);
            }
        }else{
            res.sendStatus(401);
        }
    })
}

printLink = function(link, req, res, next){
    https://tcgbinder.com/api/cardsInfo_getPrice?card_id=be8495b2-0fe8-4644-89c9-62a3f07bd7d0
    return (req, res, next)=>{
        console.log(link)
        next();
    }


}


validator = (req, res, next)=>{
    console.log("validator")


    console.log(req.body)
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        console.log(errors.array())
        return res.status(422).json({message : "missingParameters"})
    }

    next();

}

module.exports = {  authTokenConfirmed,
                    ensureToken,
                    checkIfUserExists,
                    authTokenNotConfirmed,
                    printLink,
                    validator
                };