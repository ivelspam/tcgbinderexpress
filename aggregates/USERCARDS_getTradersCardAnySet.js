db.wantedcards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    user_id : ObjectId("5c295d8638f3a611b0f4c712")
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "name",
			    "foreignField" : "name",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$project: {
			name : "$name",
			nonfoilQuantity : "$nonfoilQuantity",
			foilQuantity : "$foilQuantity",
			  wanted_ids : {
			   $map : {
			    
			      input : "$cardInfo" ,
			      as: "card",
			      in: "$$card._id"
			   } 
			     
			   } 
			  }
		},

		// Stage 4
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {wanted_ids : "$wanted_ids"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $in: [ "$card_id",  "$$wanted_ids" ] },
			                         { $in: [ "$user_id",  [ObjectId("5c29d8d538f3a611b0f4c713")] ] },
			                         { $eq: [ "$type",  "cards" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "foundCard"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$foundCard"
			}
		},

		// Stage 6
		{
			$group: {
			    _id : {_id : "$foundCard.user_id", name : "$name"},
			    traderNonfoilQuantity : {$sum : "$foundCard.nonfoilQuantity"},
			    traderFoilQuantity : {$sum : "$foundCard.foilQuantity"},
			    nonfoilQuantity : {$first : "$nonfoilQuantity"},
			    foilQuantity : {$first : "$foilQuantity"}
			}
		},

		// Stage 7
		{
			$project: {
				_id : "$_id",
			    anySetFoilQuantity : {
			        $cond : [ {$lte : [ "$traderFoilQuantity", "$foilQuantity"]} , {$ifNull : ["$traderFoilQuantity", 0]} , "$foilQuantity"]
			    },
			    anySetNonfoilQuantity : {
			        $cond : [ {$lte : [ "$traderNonfoilQuantity", "$nonfoilQuantity"]} , {$ifNull : ["$traderNonfoilQuantity", 0]} , "$nonfoilQuantity"]
			    },
			
			    
			}
		},

		// Stage 8
		{
			$group: {
			    _id : "$_id._id",
			    anySetFoilQuantity : {$sum : "$anySetFoilQuantity"},
			    anySetNonfoilQuantity : {$sum : "$anySetNonfoilQuantity"}
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
