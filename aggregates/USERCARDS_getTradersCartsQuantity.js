db.usercards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				user_id : ObjectId("5c295d8638f3a611b0f4c712"),
				type : "wanted"
			}
		},

		// Stage 2
		{
			$lookup: {
				"from" : "usercards",
				"let" : {card_id : "$card_id"},
				"pipeline" : [
					{$match :
							{ $expr:
									{ $and:
											[
												{ $eq: [ "$card_id",  "$$card_id" ] },
												{ $in: [ "$user_id",  [ObjectId("5c29d8d538f3a611b0f4c713")] ] },
												{ $eq: [ "$type",  "cards" ] }
											]
									}
							}
					}
				],
				"as" : "foundCard"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$foundCard",
			    includeArrayIndex : "arrayIndex",
			    preserveNullAndEmptyArrays : false
			}
		},

		// Stage 4
		{
			$project: {
				_id : "$foundCard.user_id",
			    foilQuantity : {
			        $cond : [ {$lte : [ "$foundCard.foilQuantity", "$foilQuantity"]} , {$ifNull : ["$foundCard.foilQuantity", 0]} , "$foilQuantity"]
			    },
			    nonfoilQuantity : {
			        $cond : [ {$lte : [ "$foundCard.nonfoilQuantity", "$nonfoilQuantity"]} , {$ifNull : ["$foundCard.nonfoilQuantity", 0]} , "$nonfoilQuantity"]
			    },
			}
		},

		// Stage 5
		{
			$group: {
				_id : "$_id",
				foilQuantity : {$sum : "$foilQuantity"},
				nonfoilQuantity : {$sum : "$nonfoilQuantity"}
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
