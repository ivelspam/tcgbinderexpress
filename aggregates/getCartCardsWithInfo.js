db.cartcards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				$or : [{wanter_id : ObjectId("5c0807a70ded3e3f84f0af42"), trader_id : ObjectId("5c051b43c4fb4645ecfc8230")}, {wanter_id : ObjectId("5c051b43c4fb4645ecfc8230") , trader_id : ObjectId("5c0807a70ded3e3f84f0af42")}]
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "card_id",
			    "foreignField" : "_id",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$cardInfo"
			}
		},

		// Stage 4
		{
			$project: {
			    card_id : 1,
			    trader_id : 1,
			    wanter_id : 1,
			    foilMyPrice : 1,
			    foilPriceType : 1,
			    foilQuantity :1 ,
			    nonfoilMyPrice : 1,
			    nonfoilPriceType : 1,
			    nonfoilQuantity : 1,
				usd : "$cardInfo.usd",
				eur : "$cardInfo.eur",
				name : "$cardInfo.name",
				set_code : "$cardInfo.set_code",
				set_name : "$cardInfo.set_name"
				
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
