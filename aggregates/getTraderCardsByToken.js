db.usercards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				user_id : ObjectId("5be33e8458e63611644ea8a3"),
				type : "cards",
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "card_id",
			    "foreignField" : "_id",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$cardInfo"
			}
		},

		// Stage 4
		{
			$match: {
				"cardInfo.name" : /.*l.*/
			}
		},

		// Stage 5
		{
			$project: {
			    card_id : 1,
			    foilQuantity : 1,
			    nonfoilQuantity : 1,
			    usd : "$cardInfo.usd",
			    eur : "$cardInfo.eur",
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
