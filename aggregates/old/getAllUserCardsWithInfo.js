db.usercards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    "user_id" : ObjectId("5be33ec4dfb86c49608807bb")
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "card_id",
			    "foreignField" : "_id",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$cardInfo"
			}
		},

		// Stage 4
		{
			$group: {
			    _id : "$user_id",
			    cards : {$push : {
			       $cond: [{"$eq": ["$type", "cards"]}, {
			         _id : "$card_id",
			         binder_id : "$binder_id",
			         foilQuantity : "$foilQuantity",
			         nonfoilQuantity : "$nonfoilQuantity",
			         usd : "$cardInfo.usd",
			            eur : "$cardInfo.eur"
			         }, false]
			    }},
			    wanted : {$push : {
			      
			       $cond: [{"$eq": ["$type", "wanted"]}, {
			            _id : "$card_id",
			         foilQuantity : "$foilQuantity",
			         nonfoilQuantity : "$nonfoilQuantity",
			         usd : "$cardInfo.usd",
			            eur : "$cardInfo.eur"
			       }, false]
			    }}
			}
		},

		// Stage 5
		{
			$project: {
				_id : 0,
				"cards": {"$setDifference": ["$cards", [false]]},
				"wanted": {"$setDifference": ["$wanted", [false]]}
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
