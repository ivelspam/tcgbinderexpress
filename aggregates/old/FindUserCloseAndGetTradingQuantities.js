db.users.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$geoNear: { near: { type: 'Point', coordinates: [ -111.9, 40.317998333333335 ] },
			distanceField: 'dist.calculated',
			maxDistance: 130329,
			query: { _id: { '$ne': ObjectId("5be33ec4dfb86c49608807bb") } },
			includeLocs: 'dist.location',
			spherical: true 
			}
		},

		// Stage 2
		{
			$project: {
			    _id : 1
			}
		},

		// Stage 3
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {user_id : "$_id"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $eq: [ "$type",  "cards" ] },    
			                         { $eq: [ "$user_id",  "$$user_id" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "tradeCard"
			}
		},

		// Stage 4
		{
			$unwind: {
			    path : "$tradeCard",
			    preserveNullAndEmptyArrays : true // optional
			    
			}
		},

		// Stage 5
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {card_id : "$tradeCard.card_id"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $eq: [ "$type",  "wanted" ] },    
			                         { $eq: [ "$user_id",  ObjectId("5be33ec4dfb86c49608807bb") ] },
			                         { $eq: [ "$card_id",  "$$card_id" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "wantedCard"
			}
		},

		// Stage 6
		{
			$unwind: {
			    path : "$wantedCard",
			    preserveNullAndEmptyArrays : true // optional
			}
		},

		// Stage 7
		{
			$project: {
			    foilQuantity : {
			        $cond : [ {$lte : [ "$wantedCard.foilQuantity", "$tradeCard.foilQuantity"]} , {$ifNull : ["$wantedCard.foilQuantity", 0]} , "$tradeCard.foilQuantity"]
			    },
			    nonfoilQuantity : {
			        $cond : [ {$lte : [ "$wantedCard.nonfoilQuantity", "$tradeCard.nonfoilQuantity"]} , {$ifNull : ["$wantedCard.nonfoilQuantity", 0]} , "$tradeCard.nonfoilQuantity"]
			    },
			
			    
			}
		},

		// Stage 8
		{
			$group: {
			    _id : "$_id",
			    foilQuantity: {$sum : "$foilQuantity"},
			    nonfoilQuantity: {$sum : "$nonfoilQuantity"},
			}
		},

		// Stage 9
		{
			$lookup: {
			    "from" : "users",
			    "localField" : "_id",
			    "foreignField" : "_id",
			    "as" : "userInfo"
			}
		},

		// Stage 10
		{
			$unwind: {
			    path : "$userInfo"
			}
		},

		// Stage 11
		{
			$project: {
			    name : "$userInfo.name",
			    foilQuantity : "$foilQuantity",
			    nonfoilQuantity : "$nonfoilQuantity",
			    longitude : { $arrayElemAt: [ "$userInfo.location.coordinates", 0 ] },
			    latitude : { $arrayElemAt: [ "$userInfo.location.coordinates", 1 ] }
			
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
