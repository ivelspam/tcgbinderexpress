db.cartcards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    $or : [{trader_id : ObjectId("5c295d8638f3a611b0f4c712")}, {wanter_id : ObjectId("5c295d8638f3a611b0f4c712")}, ]
			}
		},

		// Stage 2
		{
			$project: {
			                        trader_id : {
			                            $cond: { if: { $eq: [ "$trader_id", ObjectId("5c295d8638f3a611b0f4c712") ] }, then:"$wanter_id", else:  "$trader_id"}
			                        },
			                        type : {
			                            $cond: { if: { $eq: [ "$wanter_id", ObjectId("5c295d8638f3a611b0f4c712") ] }, then: "receiving", else: "giving" }
			                        },
			                        totalCards : {
			                            $sum : ["$foilQuantity", "$nonfoilQuantity"]
			                            
			                        },
			}
		},

		// Stage 3
		{
			$group: {
			    _id : "$trader_id",
			    receiving : {$sum : {
			        $cond :[{$eq : ["$type", "receiving"]} , "$totalCards" , 0]
			    }},
			    giving : {$sum : {
			        $cond :[{$eq : ["$type", "giving"]} , "$totalCards" , 0]
			    }},
			
			}
		},

		// Stage 4
		{
			$lookup: {
			    "from" : "users",
			    "localField" : "_id",
			    "foreignField" : "_id",
			    "as" : "traderInfo"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$traderInfo"
			}
		},

		// Stage 6
		{
			$project: {
			    receiving : 1,
			    giving : 1,
			    name : "$traderInfo.name"
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
