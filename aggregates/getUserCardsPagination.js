db.usercards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				user_id : ObjectId("5c051b43c4fb4645ecfc8230"),
			    type : "cards",
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "card_id",
			    "foreignField" : "_id",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$cardInfo"
			}
		},

		// Stage 4
		{
			$sort: {
				"cardInfo.name" : 1
			}
		},

		// Stage 5
		{
			$skip: 1
		},

		// Stage 6
		{
			$limit: 1
		},

		// Stage 7
		{
			$project: {
				_id : "$card_id",
				foilQuantity : 1,
				nonfoilQuantity : 1,
				usd : "$cardInfo.usd",
				eur : "$cardInfo.eur"
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
