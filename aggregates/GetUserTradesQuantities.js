db.usercards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			
			    user_id :  ObjectId("5c29d8d538f3a611b0f4c713"),
			    type : "cards"
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {card_id : "$card_id"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $eq: [ "$type",  "wanted" ] },    
			                         { $eq: [ "$user_id",  ObjectId("5c295d8638f3a611b0f4c712") ] },
			                         { $eq: [ "$card_id",  "$$card_id" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "wantedCard"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$wantedCard",
			    preserveNullAndEmptyArrays : true // optional
			}
		},

		// Stage 4
		{
			$project: {
			  	_id : "$user_id",
			    foilQuantity : {
			        $cond : [ {$lte : [ "$wantedCard.foilQuantity", "$foilQuantity"]} , {$ifNull : ["$wantedCard.foilQuantity", 0]} , "$foilQuantity"]
			    },
			    nonfoilQuantity : {
			        $cond : [ {$lte : [ "$wantedCard.nonfoilQuantity", "$nonfoilQuantity"]} , {$ifNull : ["$wantedCard.nonfoilQuantity", 0]} , "$nonfoilQuantity"]
			    },
			
			    
			}
		},

		// Stage 5
		{
			$group: {
			    _id : "$_id",
			    foilQuantity: {$sum : "$foilQuantity"},
			    nonfoilQuantity: {$sum : "$nonfoilQuantity"},
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
