db.wantedcards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				user_id : ObjectId("5bff239627eb103628013499")
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "name",
			    "foreignField" : "name",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$project: {
			  name : "$name",
			  nonfoilQuantity : "$nonfoilQuantity",
			  foilQuantity : "$foilQuantity",
			    wanted_ids : {
			     $map : {
			      
					input : "$cardInfo" ,
					as: "card",
					in: "$$card._id"
			     } 
			       
			     } 
			    }
			
		},

		// Stage 4
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {wanted_ids : "$wanted_ids"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $in: [ "$card_id",  "$$wanted_ids" ] },
			                         { $eq: [ "$user_id",  ObjectId("5c01396ca9ebae2e54a685ec") ] },
			                         { $eq: [ "$type",  "cards" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "foundCard"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$foundCard"
			}
		},

		// Stage 6
		{
			$match: {
				$or : [
					{nonfoilQuantity : {$gt :0}, "foundCard.nonfoilQuantity" : {$gt : 0}},
					{foilQuantity : {$gt : 0}, "foundCard.foilQuantity" : {$gt : 0}}
				]
			}
		},

		// Stage 7
		{
			$project: {
				_id : 0,
			    name : 1,
			    card_id : "$foundCard.card_id",
			    nonfoilQuantity : 1,
				foilQuantity : 1,
			    traderNonfoilQuantity : "$foundCard.nonfoilQuantity",
				traderfoilQuantity : "$foundCard.foilQuantity"
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
