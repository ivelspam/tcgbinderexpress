db.users.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$geoNear: { near: { type: 'Point', coordinates: [ 50.1, 50.1 ] },
			maxDistance: 111021,
			query: { _id: { '$ne': ObjectId("5c01396ca9ebae2e54a685ec") } },
			distanceField: "distance",
			spherical: true 
			}
		},

		// Stage 2
		{
			$project: {
			    _id : 1,
			    distance :{
				 		$ceil : {
			      			"$divide" : ["$distance", 1609]
			    		}
			    },
			    name : "name"
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
