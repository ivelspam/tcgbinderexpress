db.wantedcards.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    user_id : ObjectId("5c0807a70ded3e3f84f0af42")
			}
		},

		// Stage 2
		{
			$lookup: {
			    "from" : "cardsInfo",
			    "localField" : "name",
			    "foreignField" : "name",
			    "as" : "cardInfo"
			}
		},

		// Stage 3
		{
			$project: {
			name : "$name",
			nonfoilQuantity : "$nonfoilQuantity",
			foilQuantity : "$foilQuantity",
			  wanted_ids : {
			   $map : {
			    
			      input : "$cardInfo" ,
			      as: "card",
			      in: "$$card._id"
			   } 
			     
			   } 
			  }
		},

		// Stage 4
		{
			$lookup: {
			    "from" : "usercards",
			    "let" : {wanted_ids : "$wanted_ids"},
			    "pipeline" : [
			    {$match :
			        { $expr:
			                    { $and:
			                       [
			                         { $in: [ "$card_id",  "$$wanted_ids" ] },
			                         { $eq: [ "$user_id",  ObjectId("5c051b43c4fb4645ecfc8230") ] },
			                         { $eq: [ "$type",  "cards" ] }
			                       ]
			                    }
			        }
			    }
			    ],
			    "as" : "foundCard"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$foundCard"
			}
		},

		// Stage 6
		{
			$group: {
				_id : "$name",
				traderNonfoilQuantity : {$sum : "$foundCard.nonfoilQuantity"},
				traderFoilQuantity : {$sum : "$foundCard.foilQuantity"},
				nonfoilQuantity : {$first : "$nonfoilQuantity"},
				foilQuantity : {$first : "$foilQuantity"},
			}
		},

		// Stage 7
		{
			$project: {
			    _id : 0,
			    name : 1,
			    card_id : "$foundCard.card_id",
			    nonfoilQuantity : 1,
			    foilQuantity : 1,
			    traderNonfoilQuantity : "$foundCard.nonfoilQuantity",
			    traderfoilQuantity : "$foundCard.foilQuantity"
			}
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
