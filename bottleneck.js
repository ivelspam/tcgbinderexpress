const Bottleneck = require('bottleneck')
const request = require("request")
const cardModel = require("./mongodb/Models/CardsInfo").model
const download = require('download');
const fs = require('fs');
const { spawn } = require('child_process');


var mongoose = require("mongoose")
var cardSchema = mongoose.model(cardModel)

const cron = require("cron").CronJob


new cron({
    cronTime : "0 */4 * * *",
    onTick : ()=>{
        getAllThePrices()
    },
    onComplete : null,
    start : true
})

const limiter = new Bottleneck({
    minTime: 120
});

const getAllThePrices = ()=> {

    console.log("getAllThePrices")

    var d = new Date();
    d.setDate(d.getDate() - 1);

    cardSchema.find({priceUpdatedAt: {$lt: d}}, (err, data) => {

        console.log(`Old Prices: ${data.length}`)

        for (var i = 0; i < data.length; i++) {
            limiter.schedule(requestFunction, data[i]._id, updatePriceCallback)
        }

        limiter.schedule(() => {
            console.log("Prices Updated")
        })

    })
}

const updatePriceCallback = function(body){

    if(body != null && body.id != undefined){
        const updates = {$set : {}}

        body.usd ? updates.$set.usd = body.usd : null
        body.eur ? updates.$set.eur = body.eur : null
        updates.priceUpdatedAt = new Date()

        cardSchema.findByIdAndUpdate(body.id, updates, {new : true}, function (err, card){
            if (err) console.log(err);


        })
    }
}


const requestFunction = function(_id, callback){

    const url = 'https://api.scryfall.com/cards/' + _id

    request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        callback(body)
    });
}

const downloadsSetsJSON = function(){
    var url = `https://api.scryfall.com/sets/`;

    request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        fs.writeFile("JSONs/setsSymbols.json", JSON.stringify(body.data), 'utf-8', ()=>{
            downloadImagesFrom()

        })

    });
}





var downloadImagesFrom = function(){
    var content = fs.readFileSync("JSONs/setsSymbols.json")

    var setsSymbols = JSON.parse(content);

    for(var i = 0; i < setsSymbols.length; i++){
        const row = setsSymbols[i]
        const set = {}

        row.object != undefined ? set.object = row.object : null
        row.code != undefined ? set.code = row.code : null
        row.name != undefined ? set.name = row.name : null
        row.icon_svg_uri != undefined ? set.icon_svg_uri = row.icon_svg_uri : null

        const url = `setsSVGs/s${row.code}.svg`;

        if (!fs.existsSync(url)) {
            limiter.schedule(downloadRequestFunction, set, downloadFileCallback)
        }
    }

    limiter.schedule(convertSVG)

}

const convertSVG = function(){
    spawn("java", ['-jar', 'Svg2VectorAndroid-1.0.jar', "setsSVGs"])
}

const downloadFileCallback = function(set, data){
    fs.writeFileSync(`setsSVGs/s${set.code}.svg`, data);
}


const downloadRequestFunction = function(set, callback){
    download(set.icon_svg_uri).then(data =>{
        // callback(body)
        console.log(data)
        callback(set, data)
    })

}


module.exports = {getAllThePrices, downloadImagesFrom, downloadsSetsJSON}