const mongoose = require("mongoose")
const cartCardModelName = require("../mongodb/Models/CartCard").model
const cartCardModel = mongoose.model(cartCardModelName)

const cardsInfoModelName = require("../mongodb/Models/CardsInfo").model
const cardsInfoModel = mongoose.model(cardsInfoModelName)

const addCartCardOrUpdate = ({wanter_id, trader_id, card_id, foilQuantity, nonfoilQuantity}, callback)=>{
    console.log("addCartCardOrUpdate")

    console.log({wanter_id, trader_id, card_id, foilQuantity, nonfoilQuantity})

    const cartCardQuery = {wanter_id : wanter_id, trader_id : trader_id, card_id : card_id}

    cartCardModel.findOneAndUpdate(cartCardQuery, {foilQuantity : foilQuantity, nonfoilQuantity : nonfoilQuantity}, {upsert : true, runValidators : true, setDefaultsOnInsert : true, new : true, lean : true}, (err, cartCart)=>{
        if(err) {console.log(err)}


        cardsInfoModel.findById(card_id, {usd : 1, eur : 1, name : 1, set_code : 1, set_name : 1}, {lean : true},(error, foundCard)=>{

            let merged = {...cartCart, ...foundCard}

            console.log(merged)
            if(cartCart != null){
                callback("added", merged)
            }else{
                callback("notAdded")
            }
        })
    })

}

const getCartCards = ({user_id, trader_id}, callback)=>{
    console.log("getCartCards")

    console.log({user_id, trader_id})

    const cartCardQuery = {$or : [{wanter_id : user_id, trader_id : trader_id}, {wanter_id : trader_id , trader_id : user_id}]}

    cartCardModel.find(cartCardQuery, (err, cart)=>{
        if(err) {console.log(err)}

        console.log(cartCardQuery)
        callback("done", cart)
    })

}

const clearCart = ({user_id, trader_id}, callback)=>{

    console.log("clearCart")

    console.log({user_id, trader_id})

    const cartCardQuery = {$or : [{wanter_id : user_id, trader_id : trader_id}, {wanter_id : trader_id , trader_id : user_id}]}

    cartCardModel.deleteMany(cartCardQuery, (err)=>{
        if(err) {console.log(err)}

        callback("cleared")
    })

}

var updateCartCard = ({user_id, trader_id, updateType, cartCard}, callback)=>{
    console.log("getCartCards")


    var query = {};
    var update = {};
    if(updateType == "giving"){
        query.trader_id = user_id;
        query.wanter_id = trader_id;
        query.card_id = cartCard.card_id;
        update.foilMyPrice = cartCard.foilMyPrice
        update.foilPriceType = cartCard.foilPriceType
        update.nonfoilMyPrice = cartCard.nonfoilMyPrice
        update.nonfoilPriceType = cartCard.nonfoilPriceType

    }else if (updateType == "receiving"){
        query.trader_id = trader_id;
        query.wanter_id = user_id;
        query.card_id = cartCard.card_id;

        update.foilQuantity = cartCard.foilQuantity
        update.nonfoilQuantity = cartCard.nonfoilQuantity

    }else{
        callback("error")
        return
    }


    console.log(query)
    console.log(update)

    cartCardModel.findOneAndUpdate(query, update, {new : true, lean : true}, (err, updatedCard)=>{
        if(err) {console.log(err)}

        cardsInfoModel.findById(cartCard.card_id, {usd : 1, eur : 1, name : 1, set_code : 1, set_name : 1}, {lean : true},(error, foundCard)=>{

            let merged = {...updatedCard, ...foundCard}
            if(cartCard != null){
                callback("updated", merged)
            }else{
                callback("notUpdated")

            }
        })

    })

}

var removeCartCard = ({user_id, trader_id, updateType, cartCard}, callback)=>{
    console.log("removeCartCard")


    var query = {};
    if(updateType == "giving"){
        query.trader_id = user_id;
        query.wanter_id = trader_id;
        query.card_id = cartCard.card_id;

    }else if (updateType == "receiving"){
        query.trader_id = trader_id;
        query.wanter_id = user_id;
        query.card_id = cartCard.card_id;

    }else{
        callback("error")
        return
    }


    console.log(query)

    cartCardModel.findOneAndDelete(query, {}, (err, cartCard)=>{
        if(err) {console.log(err)}

        if(cartCard != null){
            callback("deleted", cartCard)
        }else{
            callback("notDeleted")

        }

    })

}

const getOpenCarts = ({user_id}, callback)=>{

    cartCardModel.aggregate(

            // Pipeline
            [
                // Stage 1
                {
                    $match: {
                        $or : [{trader_id :  mongoose.Types.ObjectId(user_id)}, {wanter_id : mongoose.Types.ObjectId(user_id)}, ]
                    }
                },

                // Stage 2
                {
                    $project: {
                        trader_id : {
                            $cond: { if: { $eq: [ "trader_id", mongoose.Types.ObjectId(user_id) ] }, then: "$trader_id", else: "$wanter_id" }
                        },
                        type : {
                            $cond: { if: { $eq: [ "$wanter_id", mongoose.Types.ObjectId(user_id) ] }, then: "receiving", else: "giving" }
                        },
                        totalCards : {
                            $sum : ["$foilQuantity", "$nonfoilQuantity"]

                        },
                    }
                },

                // Stage 3
                {
                    $group: {
                        _id : "$trader_id",
                        receiving : {$sum : {
                                $cond :[{$eq : ["$type", "receiving"]} , "$totalCards" , 0]
                            }},
                        giving : {$sum : {
                                $cond :[{$eq : ["$type", "giving"]} , "$totalCards" , 0]
                            }},

                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from" : "users",
                        "localField" : "_id",
                        "foreignField" : "_id",
                        "as" : "traderInfo"
                    }
                },

                // Stage 5
                {
                    $unwind: {
                        path : "$traderInfo"
                    }
                },

                // Stage 6
                {
                    $project: {
                        receiving : 1,
                        giving : 1,
                        name : "$traderInfo.name"
                    }
                },
            ],
    (err, openCarts)=>{
            if(err){console.log(err)}
            callback("done", openCarts)
        }
    );



}

module.exports = {
    addCartCardOrUpdate,
    getCartCards,
    updateCartCard,
    removeCartCard,
    getOpenCarts,
    clearCart
}
