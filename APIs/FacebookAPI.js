const mongoose = require("mongoose")
var request = require("request");

const userModelName = require("../mongodb/Models/User").model
const UserModel = mongoose.model(userModelName)

const jwt = require("jsonwebtoken")
const bcryptjs = require("bcryptjs");
const fse = require("fs-extra");
const Handlebars = require("handlebars");
const api_key = 'key-d5b74580d0e458d1355891ff8fcb61ab';
const DOMAIN = 'mg.tcgbinder.com';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
const fs = require('fs-extra')


const facebookLoginPOST = function({facebookUserToken}, callback){
    console.log("facebookLoginPOST");


    const access_token = fs.readFileSync("Keys/facebook.txt", "utf8");

    console.log(`access_token: ${access_token}`)
    const requestURL = `https://graph.facebook.com/debug_token?input_token=${facebookUserToken}&access_token=${access_token}`;
    console.log(requestURL)

    request.get(requestURL, (err, response, body)=>{
        let facebookTokenInfo = JSON.parse(body);

        if(facebookTokenInfo.data == null){
            return;
        }

        UserModel.findOne({facebookUserId : facebookTokenInfo.data.user_id}, (err, foundUser)=>{
            if(err){console.log(err)}

            if(foundUser){
                console.log("found user")
                callback("foundUser", foundUser._id)

            }else{

                request.get(`https://graph.facebook.com/me?access_token=${facebookUserToken}&fields=first_name, last_name, email, id`, (err, response, bodyFACEBOOK)=>{
                    var facebookRequestBody = JSON.parse(bodyFACEBOOK);

                    UserModel.findOne({email : facebookRequestBody.email}, (err, foundUser)=>{
                        if(err){console.log(err)}
                        if(foundUser){
                            foundUser.set({email : facebookRequestBody.email, name : facebookRequestBody.first_name, facebookUserId : facebookRequestBody.id})
                            foundUser.save((err, newUser)=>{
                                if(err){console.log(err)}
                                callback("synced", newUser._id)
                            })

                        }else{
                            var tempUser = new UserModel({email : facebookRequestBody.email, name : facebookRequestBody.first_name, facebookUserId : facebookRequestBody.id})

                            tempUser.save((err, newUser)=>{
                                if(err){console.log(err)}
                                callback("created", newUser._id)
                            })
                        }
                    })
                });
            }
        })
    });
}

module.exports = {
    facebookLoginPOST
}