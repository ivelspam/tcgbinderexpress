const modelName = require("../mongodb/Models/CardsInfo").model
const mongoose = require("mongoose")
const CardsInfoModel = mongoose.model(modelName)

const getCardPrice = function({card_id}, callback){
    console.log("getCardPrice")

    CardsInfoModel.findById(card_id, {lean : true}).select({usd : 1, eur : 1}).exec((err, price)=>{
        if(err){throw err}
        if(price != null){
            return callback(this.responses.found, price)
        }
        return callback(this.responses.notFound)
    });
}

module.exports = {getCardPrice}

