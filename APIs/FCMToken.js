const mongoose = require("mongoose")
const FCMTokenModel = mongoose.model(require("../mongodb/Models/FCMToken").model)


const updateFCMToken = function({user_id, FCMToken, deviceType}, callback){

    console.log("updateFCMToken")


    console.log({user_id, FCMToken, deviceType})

    FCMTokenModel.findOneAndUpdate({user_id : user_id, FCMToken : FCMToken, deviceType : deviceType}, {}, {upsert : true}, (err, response)=>{
        if(err){console.log(err)
            if(err){console.log(callback("message", "error"))} return
        }

        console.log(response)

        console.log(callback("message", "updated"))

    })

}

module.exports = {
    updateFCMToken
}