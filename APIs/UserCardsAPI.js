const mongoose = require("mongoose")

const UserModelName = require("../mongodb/Models/User").model
const UserSchema = mongoose.model(UserModelName)

const UserCardModelName = require("../mongodb/Models/UserCard").model
const UserCardSchema = mongoose.model(UserCardModelName)

const WantedCardModelName = require("../mongodb/Models/WantedCard").model
const WantedCardSchema = mongoose.model(WantedCardModelName)


const links = require("../Constants").links


const changeCardQuantity = function({user_id, card_id, foilQuantity, nonfoilQuantity}, callback){
    console.log("changeCardQuantity")

    UserCardSchema.findOneAndUpdate({user_id : mongoose.Types.ObjectId(user_id), card_id : card_id, card_id, type : "cards"}, {foilQuantity : foilQuantity, nonfoilQuantity : nonfoilQuantity}, {upsert : true, new: true}, (err, data)=>{
        if(err){console.log(err)}
        return callback(this.responses.updated)
    })
}

const deleteFromCards = function({user_id, card_id}, callback){
    console.log("deleteFromCards")

    UserCardSchema.findOneAndDelete({user_id : mongoose.Types.ObjectId(user_id), card_id : card_id, card_id, type : "cards"}, (err, data)=>{
        if(err){console.log(err)}
        callback(this.responses.deleted)
    })
}

const deleteWantedCard = function({user_id, card_id}, callback){
    console.log("deleteWantedCard")

    UserCardSchema.findOneAndDelete({user_id : mongoose.Types.ObjectId(user_id), card_id : card_id, card_id, type : "wanted"},  (err, data)=>{
        if(err){console.log(err)}
        callback(this.responses.wantedDeleted)
        console.log(data)
    })
}

const changeWantedQuantity = function({user_id, card_id, foilQuantity, nonfoilQuantity}, callback){
    console.log("changeCardsQuantity")

    UserCardSchema.findOneAndUpdate({user_id : mongoose.Types.ObjectId(user_id), card_id : card_id, card_id, type : "wanted"}, {foilQuantity : foilQuantity, nonfoilQuantity : nonfoilQuantity}, {upsert : true}, (err, data)=>{
        if(err){console.log(err)}
        callback(this.responses.updatedWanted, data)
    })
}

const changeWantedGeneralQuantity = function({user_id, name, foilQuantity, nonfoilQuantity}, callback){
    console.log("changeCardsQuantity")


    WantedCardSchema.findOneAndUpdate({user_id : mongoose.Types.ObjectId(user_id), name : name}, {foilQuantity : foilQuantity, nonfoilQuantity : nonfoilQuantity}, {upsert : true}, (err, data)=>{
        if(err){console.log(err)}
        callback("updatedWantedGeneral", data)
    })
}
const deleteWantedGeneralQuantity = function({user_id, name}, callback){
    console.log("deleteWantedGeneralQuantity")

    WantedCardSchema.findOneAndDelete({user_id : mongoose.Types.ObjectId(user_id), name : name}, (err, data)=>{
        if(err){console.log(err)}
        callback(this.responses.wantedGeneralDeleted, data)
    })
}

const getUserCards = function({user_id}, callback){
    console.log("changeCardsQuantity")

    WantedCardSchema.findOneAndDelete({user_id : mongoose.Types.ObjectId(user_id), name : name}, (err, data)=>{
        if(err){console.log(err)}
        callback(this.responses.wantedGeneralDeleted, data)
    })
}

const getAllUserCards = function({user_id}, callback){

    UserCardSchema.aggregate(

        // Pipeline
        [
            // Stage 1
            {
                $match: {
                    "user_id" : mongoose.Types.ObjectId(user_id)
                }
            },

            // Stage 2
            {
                $lookup: {
                    "from" : "cardsInfo",
                    "localField" : "card_id",
                    "foreignField" : "_id",
                    "as" : "cardInfo"
                }
            },

            // Stage 3
            {
                $unwind: {
                    path : "$cardInfo"
                }
            },

            // Stage 4
            {
                $group: {
                    _id : "$user_id",
                    cards : {$push : {
                            $cond: [{"$eq": ["$type", "cards"]}, {
                                _id : "$card_id",
                                binder_id : "$binder_id",
                                foilQuantity : "$foilQuantity",
                                nonfoilQuantity : "$nonfoilQuantity",
                                usd : "$cardInfo.usd",
                                eur : "$cardInfo.eur"
                            }, false]
                        }},
                    wanted : {$push : {

                            $cond: [{"$eq": ["$type", "wanted"]}, {
                                _id : "$card_id",
                                foilQuantity : "$foilQuantity",
                                nonfoilQuantity : "$nonfoilQuantity",
                                usd : "$cardInfo.usd",
                                eur : "$cardInfo.eur"
                            }, false]
                        }}
                }
            },

            // Stage 5
            {
                $project: {
                    _id : 1,
                    "cards": {"$setDifference": ["$cards", [false]]},
                    "wanted": {"$setDifference": ["$wanted", [false]]}
                }
            },
        ],
        (err, userCards)=>{
            if (err) {console.log(err)}

            WantedCardSchema.find({user_id : user_id}, (err, wantedCards)=>{

                const values = {}
                if(userCards.length){
                    Object.assign( values , userCards[0], {wantedCards : wantedCards})
                }else{
                    Object.assign( values, {cards : [] , wanted : [], wantedCards : wantedCards})
                }
                callback("gotAllCards", values); return
            })
        }
    );
}

module.exports = {
    deleteFromCards,
    deleteWantedCard,
    changeCardQuantity,
    changeWantedQuantity,
    changeWantedGeneralQuantity,
    deleteWantedGeneralQuantity,
    getAllUserCards

}