const mongoose = require("mongoose")

const userModelName = require("../mongodb/Models/User").model
const UserModel = mongoose.model(userModelName)

const userCardSchema = mongoose.model(require("../mongodb/Models/UserCard").model)
const emailOneTimeTokenSchema = mongoose.model(require("../mongodb/Models/EmailOneTimeToken").model)
const wantedCardsModel = mongoose.model(require("../mongodb/Models/WantedCard").model)
const cartCardModel = mongoose.model(require("../mongodb/Models/CartCard").model)
const chatmessagesModel = mongoose.model(require("../mongodb/Models/ChatMessage").model)
const wantedcardsModel = mongoose.model(require("../mongodb/Models/WantedCard").model)
const FCMToken = mongoose.model(require("../mongodb/Models/FCMToken").model)


const jwt = require("jsonwebtoken")
const bcryptjs = require("bcryptjs");
const fse = require("fs-extra");
const Handlebars = require("handlebars");
const api_key = 'key-d5b74580d0e458d1355891ff8fcb61ab';
const DOMAIN = 'mg.tcgbinder.com';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});

var addUserWithPassword = function({email, name, password}, callback){



    console.log("addUserWithPassword");
    let passHash = bcryptjs.hashSync(password, bcryptjs.genSaltSync(10));

    UserModel.findOne({email : email, password : {$ne : null}}, (err, data)=>{
        if(err)console.log(err)

        if(data == null || data.accountStatus == 2){
            createNewPasswordAccount({email : email, name : name, passHash : passHash}, (response, user)=>{
                console.log(user)
                callback(response, user)
                // utils.makeJSONResponse(res, "registered", payload, "Keys/notConfirmed.key", {info : user});
                // sendUserEmailConfirmation(user)
            })
        }else if(data.facebookUserId != undefined && data.password == undefined){
            console.log("33333333333333333333333")
            syncWithFacebookAcccount(data, passHash, (response, user)=>{
                var payload = {user_id : user._id, accountStatus : 1, login: "email"};
                callback("registered", info)
                // utils.makeJSONResponse(res, "registered", payload, "Keys/notConfirmed.key", {info : user});
            })
        }else{
            callback("accountExists")
        }
    })
}


var createNewPasswordAccount = ({email, name, passHash}, callback)=>{

    console.log("createNewPasswordAccount")
    console.log(email, name, passHash)

    UserModel.deleteOne({email : email}, (err)=>{
        var tempSchema = new UserModel({email : email, name : name, password : passHash})

        tempSchema.save((err, data)=>{
            if(err){console.log(err)}
            callback("registered", data)
        })
    })

}

var syncWithFacebookAcccount = (user, password, callback)=>{
    UserModel.findByIdAndUpdate(user.id, {password : password} ,(err, data)=>{
        if(err){console.log(err)}
        callback("registered", data)
    })
}

var sendUserEmailConfirmation = (user)=>{

    var emailPayload = {user_id : user._id, type : "new", exp : getExpirationTime()};
    var jwtToken = jwt.sign(emailPayload, fse.readFileSync("Keys/login.key", "utf-8"));
    var template = fse.readFileSync("templates/emailConfirmation/emailConfirmation.hbs", "utf-8");
    var compiledTemplate = Handlebars.compile(template);
    var result2 = compiledTemplate({name : user.name, token : jwtToken})

    var data = {
        from: 'TCGBinder <ivelacc@tcgbinder.com>',
        to: user.email,
        subject: 'SyncHobby Confirmation',
        html: result2
    };

    mailgun.messages().send(data, function (error, body) {
        if(error){console.log(error)}
    });

}

var getExpirationTime = ()=>{
    return Date.now() + 2 * 60 * 60 * 1000
}

var confirmEmailGET = function(token, callback){
    console.log("confirmEmailGET")

    jwt.verify(token, fse.readFileSync("Keys/login.key", "utf8"), function(err, tokenData){
        if(err){ console.log(err); callback(403); return }
        if(tokenData.type != "new"){  callback(403); return }

        emailOneTimeTokenSchema.findOne({token : token}, (err, data)=>{
            if(err) throw err

            console.log(data)

            if(data) { callback("tokenAlreadyUsed"); return}
            UserModel.findOne({_id : tokenData.user_id}, (err, userFound)=>{
                if(err) throw err
                if(userFound == null){ callback("userNotFound"); return; }
                if(userFound.emailConfirmed == 1){callback("userAlreadyConfirmed"); return; }

                UserModel.findByIdAndUpdate(tokenData.user_id, {emailConfirmed : 1, accountStatus : 1}, (err, data)=>{
                    if(err) throw err;

                    callback("emailConfirmed");

                    var tempEmailOneTimeTokenSchema = new emailOneTimeTokenSchema({token : token})
                    tempEmailOneTimeTokenSchema.save((err, data)=>{
                        if(err) throw err;
                    })
                })
            })
        })
    })
}


var emailLoginUser = function({email, password}, callback){

    UserModel.findOne({email : email, password : {$exists : true}},  (err, foundUser)=> {
        if (err) throw err;
        if(foundUser == null){
            callback("noMatch")
            return
        }
        bcryptjs.compare(password, foundUser.password, (err, response) => {
            console.log(response)
            if(response){callback(this.responses.match, foundUser); return}
            callback(this.responses.noMatch, ); return
        })
    })
}

var getUserFirstLoginInfo = (_id, callback)=>{
    console.log("getUserFirstLoginInfo")

    UserModel.findById(_id).select({password : 0, accountStatus : 0, location : 0, createdAt : 0, _id : 0}).exec((err, foundUser)=> {
        if (err) err;
        if(foundUser == null){callback("userNotFound")}
        callback("foundUser", foundUser); return
    })
}


var changeUserDistanceAndMeasurementType = ({user_id, distance, measurementSystem}, callback)=>{
        UserModel.findByIdAndUpdate(user_id, {distance : distance, measurementSystem : measurementSystem}, {lean:true}, (err, data)=>{
            if(err){console.log(err)}
            // console.log(data)
            callback("updated")
        })
}


var changeOpenForTrading = ({user_id, openForTrading, latitude, longitude}, callback)=>{

    console.log('changeOpenForTrading')
    console.log(openForTrading)


    console.log({user_id, openForTrading, latitude, longitude})

    const fixedCoordinates = Coordinate(latitude, longitude)

    console.log(fixedCoordinates)

    UserModel.findByIdAndUpdate(user_id, {openForTrading : openForTrading, "location.coordinates" : [fixedCoordinates.longitude, fixedCoordinates.latitude]}, {lean:true}, (err, data)=>{
        if(err){console.log(err)}
        // console.log(data)
        if(openForTrading){
            callback("changed")
        }
    })
}

const Coordinate = (latitude, longitude)=>
{
    const bearing = Math.random() * 360;
    const distance = 50;


    const R = 6371000; // meters , earth Radius approx
    const PI = 3.1415926535;
    const RADIANS = PI/180;
    const DEGREES = 180/PI;

    const lat1 = latitude * RADIANS;
    const lon1 = longitude * RADIANS;
    const radbear = bearing * RADIANS;

    // System.out.println("lat1="+lat1 + ",lon1="+lon1);

    const lat2 = Math.asin( Math.sin(lat1)*Math.cos(distance / R) +
        Math.cos(lat1)*Math.sin(distance/R)*Math.cos(radbear) );
    const lon2 = lon1 + Math.atan2(Math.sin(radbear)*Math.sin(distance / R)*Math.cos(lat1),
        Math.cos(distance/R)-Math.sin(lat1)*Math.sin(lat2));

    // System.out.println("lat2="+lat2*DEGREES + ",lon2="+lon2*DEGREES);
    return {longitude : lon2 * DEGREES, latitude : lat2 * DEGREES}
}

var updateFCMToken = ({_id, deviceType, FCMToken}, callback)=> {



}

var getUserWithCardsPrices = ({_id, latitude, longitude}, callback)=>{




    UserModel.findByIdAndUpdate(_id, {"location.coordinates" : [longitude, latitude]}, {lean : true, new : true}).select({accountStatus : 0, location : 0, emailConfirmed: 0, password : 0, createdAt :0 , __v : 0}).exec((err, foundUser)=>{
        if(err) {throw err}
        userCardSchema.aggregate(

                // Pipeline
                [
                    // Stage 1
                    {
                        $match: {
                            "user_id" : mongoose.Types.ObjectId(_id)
                        }
                    },

                    // Stage 2
                    {
                        $lookup: {
                            "from" : "cardsInfo",
                            "localField" : "card_id",
                            "foreignField" : "_id",
                            "as" : "cardInfo"
                        }
                    },

                    // Stage 3
                    {
                        $unwind: {
                            path : "$cardInfo"
                        }
                    },

                    // Stage 4
                    {
                        $group: {
                            _id : "$user_id",
                            cards : {$push : {
                                    $cond: [{"$eq": ["$type", "cards"]}, {
                                        _id : "$card_id",
                                        binder_id : "$binder_id",
                                        foilQuantity : "$foilQuantity",
                                        nonfoilQuantity : "$nonfoilQuantity",
                                        usd : "$cardInfo.usd",
                                        eur : "$cardInfo.eur"
                                    }, false]
                                }},
                            wanted : {$push : {

                                    $cond: [{"$eq": ["$type", "wanted"]}, {
                                        _id : "$card_id",
                                        foilQuantity : "$foilQuantity",
                                        nonfoilQuantity : "$nonfoilQuantity",
                                        usd : "$cardInfo.usd",
                                        eur : "$cardInfo.eur"
                                    }, false]
                                }}
                        }
                    },

                    // Stage 5
                    {
                        $project: {
                            _id : 1,
                            "cards": {"$setDifference": ["$cards", [false]]},
                            "wanted": {"$setDifference": ["$wanted", [false]]}
                        }
                    },
            ],
         (err, userCards)=>{
                if (err) {console.log(err)}

             wantedCardsModel.find({user_id : _id}, (err, wantedCards)=>{
                 if(userCards.length){
                     Object.assign(foundUser, userCards[0], {wantedCards : wantedCards})
                 }else{
                     Object.assign(foundUser, {cards : [] , wanted : [], wantedCards : wantedCards})
                 }
                 callback("userFound", foundUser); return
             })
            }
        );
    })
}

const updateTradeInfo = function({user_id, tradeInfo}, callback){


    console.log("updateTradeInfo")

    console.log(user_id, tradeInfo)

    UserModel.findByIdAndUpdate(user_id, {tradeInfo : tradeInfo}, {new : true}, (err, changes)=>{
        if(err){console.log(err)}

        console.log(changes)
        if(changes != null){
            callback(this.responses.updated)
        }else {
            callback(this.responses.error)
        }
    })
}

const logOut = function({}, callback){

    console.log("logOut")

    callback("loggedout")

    // UserModel.findByIdAndUpdate(user_id, {tradeInfo : tradeInfo}, {new : true}, (err, changes)=>{
    //     if(err){console.log(err)}
    //
    //     console.log(changes)
    //     if(changes != null){
    //         callback(this.responses.updated)
    //     }else {
    //         callback(this.responses.error)
    //     }
    // })
}


const updateLocation = function({user_id, longitude, latitude}, callback){
    const newCoordinated = UpdatedCoordinate({longitude : longitude, latitude : latitude})

    console.log(newCoordinated)

    console.log(this)

    UserModel.findByIdAndUpdate(user_id, {"location.coordinates[0]" : newCoordinated.longitude, "location.coordinates[1]" : newCoordinated.latitude}, {new : true}, (err, changes)=>{
        if(err){console.log(latitude)}
        if(changes != null){
            callback(this.responses.updated)
        }else {
            callback(this.responses.error)
        }
    })
}

const UpdatedCoordinate = ({latitude, longitude})=>
{
    const bearing = Math.random() * 360;
    const distance = 300 + 600 * Math.random();

    const R = 6371000; // meters , earth Radius approx
    const PI = 3.1415926535;
    const RADIANS = PI/180;
    const DEGREES = 180/PI;

    const lat1 = latitude * RADIANS;
    const lon1 = longitude * RADIANS;
    const radbear = bearing * RADIANS;

    // System.out.println("lat1="+lat1 + ",lon1="+lon1);

    const lat2 = Math.asin( Math.sin(lat1)*Math.cos(distance / R) +
        Math.cos(lat1)*Math.sin(distance/R)*Math.cos(radbear) );
    const lon2 = lon1 + Math.atan2(Math.sin(radbear)*Math.sin(distance / R)*Math.cos(lat1),
        Math.cos(distance/R)-Math.sin(lat1)*Math.sin(lat2));

    // System.out.println("lat2="+lat2*DEGREES + ",lon2="+lon2*DEGREES);
    return {longitude : lon2 * DEGREES, latitude : lat2 * DEGREES}
}

const deleteAccount = function({user_id}, callback){
    UserModel.findByIdAndDelete(user_id, (err, result)=>{
        userCardSchema.deleteMany({user_id : user_id}, (err, result)=>{
            cartCardModel.deleteMany({$or : [{trader_id : user_id}, {wanter_id : user_id}]}, (err, result)=>{
                chatmessagesModel.deleteMany({$or : [{receiver_id : user_id}, {sender_id : user_id}]}, (err, result)=>{
                    wantedcardsModel.deleteMany({user_id : user_id}, (err, result)=>{
                        callback(this.responses.accountDeleted)
                    })
                })
            })
        })
    })
}

module.exports = {
    changeUserDistanceAndMeasurementType,
    getUserWithCardsPrices,
    addUserWithPassword,
    confirmEmailGET,
    emailLoginUser,
    getUserFirstLoginInfo,
    changeOpenForTrading,
    updateLocation,
    updateTradeInfo,
    logOut,
    deleteAccount,
    updateFCMToken
}