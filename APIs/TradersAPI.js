const mongoose = require("mongoose")
const userModelName = require("../mongodb/Models/User").model
const userCardModelName = require("../mongodb/Models/UserCard").model
const UserSchema = mongoose.model(userModelName)
const userCardSchema = mongoose.model(userCardModelName)

const ChatMessageModelName = require("../mongodb/Models/ChatMessage").model

const ChatMessageModel = mongoose.model(ChatMessageModelName)

const cartCardModelName = require("../mongodb/Models/CartCard").model
const cartCardModel = mongoose.model(cartCardModelName)

const wantedCardName = require("../mongodb/Models/WantedCard").model
const wantedCardModel = mongoose.model(wantedCardName)



const getInCommonQuantity = ({user_id, trader_id}, callback)=>{
    console.log("getInCommonQuantity")
    console.log(user_id, trader_id)

    userCardSchema.aggregate(
            [
                {
                    $match: {
                        user_id :  mongoose.Types.ObjectId(trader_id),
                        type : "cards"
                    }
                },
                {
                    $lookup: {
                        "from" : "usercards",
                        "let" : {card_id : "$card_id"},
                        "pipeline" : [
                            {$match :
                                    { $expr:
                                            { $and:
                                                    [
                                                        { $eq: [ "$type",  "wanted" ] },
                                                        { $eq: [ "$user_id",  mongoose.Types.ObjectId(user_id) ] },
                                                        { $eq: [ "$card_id",  "$$card_id" ] }
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as" : "wantedCard"
                    }
                },

                // Stage 3
                {
                    $unwind: {
                        path : "$wantedCard",
                        preserveNullAndEmptyArrays : true
                    }
                },

                // Stage 4
                {
                    $project: {
                        _id : "$user_id",
                        foilQuantity : {
                            $cond : [ {$lte : [ "$wantedCard.foilQuantity", "$foilQuantity"]} , {$ifNull : ["$wantedCard.foilQuantity", 0]} , "$foilQuantity"]
                        },
                        nonfoilQuantity : {
                            $cond : [ {$lte : [ "$wantedCard.nonfoilQuantity", "$nonfoilQuantity"]} , {$ifNull : ["$wantedCard.nonfoilQuantity", 0]} , "$nonfoilQuantity"]
                        },


                    }
                },

                // Stage 5
                {
                    $group: {
                        _id : "$_id",
                        foilQuantity: {$sum : "$foilQuantity"},
                        nonfoilQuantity: {$sum : "$nonfoilQuantity"},
                    }
                },
            ],

    (err, setCardQuantity)=> {

        wantedCardModel.aggregate(
            // Pipeline
            [
                // Stage 1
                {
                    $match: {
                        user_id: mongoose.Types.ObjectId(user_id),
                    }
                },

                // Stage 2
                {
                    $lookup: {
                        "from": "cardsInfo",
                        "localField": "name",
                        "foreignField": "name",
                        "as": "cardInfo"
                    }
                },

                // Stage 3
                {
                    $project: {
                        name: "$name",
                        nonfoilQuantity: "$nonfoilQuantity",
                        foilQuantity: "$foilQuantity",
                        wanted_ids: {
                            $map: {

                                input: "$cardInfo",
                                as: "card",
                                in: "$$card._id"
                            }

                        }
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "usercards",
                        "let": {wanted_ids: "$wanted_ids"},
                        "pipeline": [
                            {
                                $match:
                                    {
                                        $expr:
                                            {
                                                $and:
                                                    [
                                                        {$in: ["$card_id", "$$wanted_ids"]},
                                                        {$eq: ["$user_id", mongoose.Types.ObjectId(trader_id),]},
                                                        {$eq: ["$type", "cards"]}
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as": "foundCard"
                    }
                },

                // Stage 5
                {
                    $unwind: {
                        path: "$foundCard"
                    }
                },

                // Stage 6
                {
                    $group: {
                        _id: "$name",
                        traderNonfoilQuantity: {$sum: "$foundCard.nonfoilQuantity"},
                        traderFoilQuantity: {$sum: "$foundCard.foilQuantity"},
                        nonfoilQuantity: {$first: "$nonfoilQuantity"},
                        foilQuantity: {$first: "$foilQuantity"},
                    }
                },

                // Stage 7
                {
                    $project: {
                        _id: "$user_id",
                        anySetFoilQuantity: {
                            $cond: [{$lte: ["$traderFoilQuantity", "$foilQuantity"]}, {$ifNull: ["$traderFoilQuantity", 0]}, "$foilQuantity"]
                        },
                        anySetNonfoilQuantity: {
                            $cond: [{$lte: ["$traderNonfoilQuantity", "$nonfoilQuantity"]}, {$ifNull: ["$traderNonfoilQuantity", 0]}, "$nonfoilQuantity"]
                        },


                    }
                },

                // Stage 8
                {
                    $group: {
                        _id: null,
                        anySetFoilQuantity: {$sum: "$anySetFoilQuantity"},
                        anySetNonfoilQuantity: {$sum: "$anySetNonfoilQuantity"},
                    }
                },
            ],

            (err, anySetQuantity) => {
                if (err) {
                    console.log(err)
                }


                var values = {}


                if (setCardQuantity.length) {
                    values = {...setCardQuantity[0]}
                }

                console.log(anySetQuantity)

                if (anySetQuantity.length) {
                    values.anySetFoilQuantity = anySetQuantity[0].anySetFoilQuantity
                    values.anySetNonfoilQuantity = anySetQuantity[0].anySetNonfoilQuantity
                }
                callback("found", values)
            });
    })
}




const getInCommonCards = ({user_id, trader_id}, callback)=>{

    console.log("getInCommonCards")

    userCardSchema.aggregate(

            // Pipeline
            [
                // Stage 1
                {
                    $match: {
                        user_id : mongoose.Types.ObjectId(user_id)
                    }
                },

                // Stage 2
                {
                    $lookup: {
                        "from" : "usercards",
                        "let" : {card_id : "$card_id"},
                        "pipeline" : [
                            {$match :
                                    { $expr:
                                            { $and:
                                                    [
                                                        { $eq: [ "$card_id",  "$$card_id" ] },
                                                        { $eq: [ "$user_id",  mongoose.Types.ObjectId(trader_id)] },
                                                        { $ne: [ "$user_id",  mongoose.Types.ObjectId(user_id)]}
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as" : "foundCard"
                    }
                },

                // Stage 3
                {
                    $unwind: {
                        path : "$foundCard",
                    }
                },

                // Stage 4
                {
                    $match: {
                        $expr : {
                            $or : [
                                {
                                    $and :
                                        [
                                            {$eq : ["$type", "cards"]},
                                            {$eq : ["$foundCard.type", "wanted"]},
                                        ]
                                },
                                {
                                    $and :
                                        [
                                            {$eq : ["$type", "wanted"]},
                                            {$eq : ["$foundCard.type", "cards"]},
                                        ]
                                }

                            ]

                        }
                    }
                },

                // Stage 5
                {
                    $project: {
                        _id : "$user_id",
                        type : { $cond: [ {$eq : ["$type", "wanted"]}, "wanted", "trade" ]},
                        card_id : "$card_id",
                        foilQuantity : {
                            $cond : [ {$lte : [ "$foundCard.foilQuantity", "$foilQuantity"]} , {$ifNull : ["$foundCard.foilQuantity", 0]} , "$foilQuantity"]
                        },
                        nonfoilQuantity : {
                            $cond : [ {$lte : [ "$foundCard.nonfoilQuantity", "$nonfoilQuantity"]} , {$ifNull : ["$foundCard.nonfoilQuantity", 0]} , "$nonfoilQuantity"]
                        },
                    }
                },

                // Stage 6
                {
                    $group: {
                        _id : "$_id",
                        trade : {$push : {
                                $cond : [{$eq : ["$type", "trade"]}, {card_id : "$card_id", foilQuantity : "$foilQuantity", nonfoilQuantity : "$nonfoilQuantity"}, null]

                            } },
                        wanted : {$push : {
                                $cond : [{$eq : ["$type", "wanted"]}, {card_id : "$card_id", foilQuantity : "$foilQuantity", nonfoilQuantity : "$nonfoilQuantity"}, null]

                            } }
                    }
                },

                // Stage 7
                {
                    $project: {
                        trade : {
                            $filter: {
                                input: "$trade",
                                as: "card",
                                cond: { $ne: [ "$$card", null ] }
                            }
                        },
                        wanted : {
                            $filter: {
                                input: "$wanted",
                                as: "card",
                                cond: { $ne: [ "$$card", null ] }
                            }
                        }
                    }
                },
            ],

        (err, data)=>{
            if(err){console.log(err)}

            console.log(data)
            if(data.length){
                callback("found", data[0])
            }else{
                callback("notFound")
            }
        });
}

const getCloserTradersWithInfo = function({user_id, distance, longitude, latitude}, callback){

    console.log("getCloserTradersAndOpenCarts")

    console.log(user_id, distance, longitude, latitude)
    UserSchema.aggregate(
        [
            {
                $geoNear: { near: { type: 'Point', coordinates: [ longitude, latitude ] },
                    maxDistance: distance * 1609,
                    query: { _id: { '$ne': mongoose.Types.ObjectId(user_id) }, openForTrading : true },
                    distanceField: "distance",
                    spherical: true
                }
            },
            {
                $project: {
                    _id : 1,
                    distance :{
                        $ceil : {
                            "$divide" : ["$distance", 1609]
                        }
                    },
                    name : 1,
                    tradeInfo : 1
                }
            },
        ],
        (err, closerTraders)=> {
            if (err) {
                console.log(err)
            }

            console.log("Got CloserTraders")

            var traders = closerTraders.map(x => mongoose.Types.ObjectId(x._id))

            userCardSchema.aggregate(
                // Pipeline
                [
                    // Stage 1
                    {
                        $match: {
                            user_id: mongoose.Types.ObjectId(user_id),
                            type: "wanted"
                        }
                    },

                    // Stage 2
                    {
                        $lookup: {
                            "from": "usercards",
                            "let": {card_id: "$card_id"},
                            "pipeline": [
                                {
                                    $match:
                                        {
                                            $expr:
                                                {
                                                    $and:
                                                        [
                                                            {$eq: ["$card_id", "$$card_id"]},
                                                            {$in: ["$user_id", traders]},
                                                            {$eq: ["$type", "cards"]}
                                                        ]
                                                }
                                        }
                                }
                            ],
                            "as": "foundCard"
                        }
                    },

                    // Stage 3
                    {
                        $unwind: {
                            path: "$foundCard",
                            includeArrayIndex: "arrayIndex",
                            preserveNullAndEmptyArrays: false
                        }
                    },

                    // Stage 4
                    {
                        $project: {
                            _id: "$foundCard.user_id",
                            foilQuantity: {
                                $cond: [{$lte: ["$foundCard.foilQuantity", "$foilQuantity"]}, {$ifNull: ["$foundCard.foilQuantity", 0]}, "$foilQuantity"]
                            },
                            nonfoilQuantity: {
                                $cond: [{$lte: ["$foundCard.nonfoilQuantity", "$nonfoilQuantity"]}, {$ifNull: ["$foundCard.nonfoilQuantity", 0]}, "$nonfoilQuantity"]
                            },
                        }
                    },

                    // Stage 5
                    {
                        $group: {
                            _id: "$_id",
                            foilQuantity: {$sum: "$foilQuantity"},
                            nonfoilQuantity: {$sum: "$nonfoilQuantity"}
                        }
                    },
                ],
                (err, setCardsQuantity) => {
                    if (err) {
                        console.log(err)
                    }
                    console.log("Got setCardsQuantity")

                    wantedCardModel.aggregate(
                        // Pipeline
                        [
                            // Stage 1
                            {
                                $match: {
                                    user_id: mongoose.Types.ObjectId(user_id)
                                }
                            },

                            // Stage 2
                            {
                                $lookup: {
                                    "from": "cardsInfo",
                                    "localField": "name",
                                    "foreignField": "name",
                                    "as": "cardInfo"
                                }
                            },

                            // Stage 3
                            {
                                $project: {
                                    name: "$name",
                                    nonfoilQuantity: "$nonfoilQuantity",
                                    foilQuantity: "$foilQuantity",
                                    wanted_ids: {
                                        $map: {

                                            input: "$cardInfo",
                                            as: "card",
                                            in: "$$card._id"
                                        }

                                    }
                                }
                            },

                            // Stage 4
                            {
                                $lookup: {
                                    "from": "usercards",
                                    "let": {wanted_ids: "$wanted_ids"},
                                    "pipeline": [
                                        {
                                            $match:
                                                {
                                                    $expr:
                                                        {
                                                            $and:
                                                                [
                                                                    {$in: ["$card_id", "$$wanted_ids"]},
                                                                    {$in: ["$user_id", traders]},
                                                                    {$eq: ["$type", "cards"]}
                                                                ]
                                                        }
                                                }
                                        }
                                    ],
                                    "as": "foundCard"
                                }
                            },

                            // Stage 5
                            {
                                $unwind: {
                                    path: "$foundCard"
                                }
                            },

                            // Stage 6
                            {
                                $group: {
                                    _id: {_id: "$foundCard.user_id", name: "$name"},
                                    traderNonfoilQuantity: {$sum: "$foundCard.nonfoilQuantity"},
                                    traderFoilQuantity: {$sum: "$foundCard.foilQuantity"},
                                    nonfoilQuantity: {$first: "$nonfoilQuantity"},
                                    foilQuantity: {$first: "$foilQuantity"}
                                }
                            },

                            // Stage 7
                            {
                                $project: {
                                    _id: "$_id",
                                    anySetFoilQuantity: {
                                        $cond: [{$lte: ["$traderFoilQuantity", "$foilQuantity"]}, {$ifNull: ["$traderFoilQuantity", 0]}, "$foilQuantity"]
                                    },
                                    anySetNonfoilQuantity: {
                                        $cond: [{$lte: ["$traderNonfoilQuantity", "$nonfoilQuantity"]}, {$ifNull: ["$traderNonfoilQuantity", 0]}, "$nonfoilQuantity"]
                                    },


                                }
                            },

                            // Stage 8
                            {
                                $group: {
                                    _id: "$_id._id",
                                    anySetFoilQuantity: {$sum: "$anySetFoilQuantity"},
                                    anySetNonfoilQuantity: {$sum: "$anySetNonfoilQuantity"}
                                }
                            },
                        ],

                        (err, anyCardsQuantity) => {
                            if (err) { console.log(err)}
                            console.log("Got anyCardsQuantity")


                            cartCardModel.aggregate(

                                [
                                    {
                                        $match: {
                                            $or : [{trader_id : mongoose.Types.ObjectId(user_id)}, {wanter_id : mongoose.Types.ObjectId(user_id)}, ]
                                        }
                                    },
                                    {
                                        $project: {
                                            trader_id : {
                                                $cond: { if: { $eq: [ "$trader_id", mongoose.Types.ObjectId(user_id) ] }, then:"$wanter_id", else:  "$trader_id"}
                                            },
                                            type : {
                                                $cond: { if: { $eq: [ "$wanter_id", mongoose.Types.ObjectId(user_id) ] }, then: "receiving", else: "giving" }
                                            },
                                            totalCards : {
                                                $sum : ["$foilQuantity", "$nonfoilQuantity"]
                                            },
                                        }
                                    },
                                    {
                                        $group: {
                                            _id : "$trader_id",
                                            receiving : {$sum : {
                                                    $cond :[{$eq : ["$type", "receiving"]} , "$totalCards" , 0]
                                                }},
                                            giving : {$sum : {
                                                    $cond :[{$eq : ["$type", "giving"]} , "$totalCards" , 0]
                                                }},

                                        }
                                    },
                                    {
                                        $lookup: {
                                            "from" : "users",
                                            "localField" : "_id",
                                            "foreignField" : "_id",
                                            "as" : "traderInfo"
                                        }
                                    },
                                    {
                                        $unwind: {
                                            path : "$traderInfo"
                                        }
                                    },
                                    {
                                        $project: {
                                            receiving : 1,
                                            giving : 1,
                                            name : "$traderInfo.name"
                                        }
                                    },
                                ],
                                (err, openCarts)=>{
                                    if(err){console.log(err)}
                                    console.log("Got openCarts")
                                    callback(this.responses.gotAllTradersInfo, {closerTraders : closerTraders, setCardsQuantity : setCardsQuantity, anyCardsQuantity : anyCardsQuantity, openCarts: openCarts})
                                }
                            );
                        })
                }
            );
        })
}


const getCloserTradersAndOpenCarts = function({user_id, distance, longitude, latitude}, callback){

    console.log("getCloserTradersAndOpenCarts")

    console.log(user_id, distance, longitude, latitude)
    UserSchema.aggregate(
        [
            {
                $geoNear: { near: { type: 'Point', coordinates: [ longitude, latitude ] },
                    maxDistance: distance * 1609,
                    query: { _id: { '$ne': mongoose.Types.ObjectId(user_id) }, openForTrading : true },
                    distanceField: "distance",
                    spherical: true
                }
            },
            {
                $project: {
                    _id : 1,
                    distance :{
                        $ceil : {
                            "$divide" : ["$distance", 1609]
                        }
                    },
                    name : 1,
                    tradeInfo : 1
                }
            },
        ],
        (err, closerTraders)=>{
            if(err){console.log(err)}

            cartCardModel.aggregate(

                    [
                        {
                            $match: {
                                $or : [{trader_id : mongoose.Types.ObjectId(user_id)}, {wanter_id : mongoose.Types.ObjectId(user_id)}, ]
                            }
                        },
                        {
                            $project: {
                                trader_id : {
                                    $cond: { if: { $eq: [ "$trader_id", mongoose.Types.ObjectId(user_id) ] }, then:"$wanter_id", else:  "$trader_id"}
                                },
                                type : {
                                    $cond: { if: { $eq: [ "$wanter_id", mongoose.Types.ObjectId(user_id) ] }, then: "receiving", else: "giving" }
                                },
                                totalCards : {
                                    $sum : ["$foilQuantity", "$nonfoilQuantity"]
                                },
                            }
                        },
                        {
                            $group: {
                                _id : "$trader_id",
                                receiving : {$sum : {
                                        $cond :[{$eq : ["$type", "receiving"]} , "$totalCards" , 0]
                                    }},
                                giving : {$sum : {
                                        $cond :[{$eq : ["$type", "giving"]} , "$totalCards" , 0]
                                    }},

                            }
                        },
                        {
                            $lookup: {
                                "from" : "users",
                                "localField" : "_id",
                                "foreignField" : "_id",
                                "as" : "traderInfo"
                            }
                        },
                        {
                            $unwind: {
                                path : "$traderInfo"
                            }
                        },
                        {
                            $project: {
                                receiving : 1,
                                giving : 1,
                                name : "$traderInfo.name"
                            }
                        },
                    ],
                (err, openCarts)=>{
                    if(err){console.log(err)}
                        callback(this.responses.gotAllTradersInfo, {closerTraders : closerTraders, openCarts : openCarts})
                }
            );
        }
    );
}

const getCloserTraders = ({user_id, distance, longitude, latitude}, callback)=>{
    console.log("getCloserTraders")

    UserSchema.aggregate(
        [
            {
                $geoNear: { near: { type: 'Point', coordinates: [ longitude, latitude ] },
                    maxDistance: distance * 1609,
                    query: { _id: { '$ne': mongoose.Types.ObjectId(user_id) }, openForTrading : true },
                    distanceField: "distance",
                    spherical: true
                }
            },
            {
                $project: {
                    _id : 1,
                    distance :{
                        $ceil : {
                            "$divide" : ["$distance", 1609]
                        }
                    },
                    name : 1,
                    tradeInfo : 1
                }
            },
        ],
        (err, closeTraders)=>{
            if(err){console.log(err)}
            callback("done", closeTraders)


        }
    );
}

const getGeneralInCommonCards = ({user_id, trader_id}, callback)=>{

    console.log("getGeneralInCommonCards")
    console.log(user_id, trader_id)

    const getWantedCards = ({user_id, trader_id}, callback)=>{
        wantedCardModel.aggregate(
                [
                    {
                        $match: {
                            user_id : mongoose.Types.ObjectId(user_id)
                        }
                    },
                    {
                        $lookup: {
                            "from" : "cardsInfo",
                            "localField" : "name",
                            "foreignField" : "name",
                            "as" : "cardInfo"
                        }
                    },
                    {
                        $project: {
                            name : "$name",
                            nonfoilQuantity : "$nonfoilQuantity",
                            foilQuantity : "$foilQuantity",
                            wanted_ids : {
                                $map : {

                                    input : "$cardInfo" ,
                                    as: "card",
                                    in: "$$card._id"
                                }

                            }
                        }

                    },
                    {
                        $lookup: {
                            "from" : "usercards",
                            "let" : {wanted_ids : "$wanted_ids"},
                            "pipeline" : [
                                {$match :
                                        { $expr:
                                                { $and:
                                                        [
                                                            { $in: [ "$card_id",  "$$wanted_ids" ] },
                                                            { $eq: [ "$user_id",  mongoose.Types.ObjectId(trader_id) ] },
                                                            { $eq: [ "$type",  "cards" ] }
                                                        ]
                                                }
                                        }
                                }
                            ],
                            "as" : "foundCard"
                        }
                    },
                    {
                        $unwind: {
                            path : "$foundCard"
                        }
                    },
                    {
                        $match: {
                            $or : [
                                {nonfoilQuantity : {$gt :0}, "foundCard.nonfoilQuantity" : {$gt : 0}},
                                {foilQuantity : {$gt : 0}, "foundCard.foilQuantity" : {$gt : 0}}
                            ]
                        }
                    },
                    {
                        $project: {
                            name : 1,
                            card_id : "$foundCard.card_id",
                            nonfoilQuantity : 1,
                            foilQuantity : 1,
                            traderNonfoilQuantity : "$foundCard.nonfoilQuantity",
                            traderFoilQuantity : "$foundCard.foilQuantity"
                        }
                    },
                ],

        (err, wantedCardsGeneral)=>{
                if(err){ console.log(wantedCardsGeneral) }
                callback(wantedCardsGeneral)
            }
        )}


    const getTradersWantedCards = ({user_id, trader_id}, callback)=> {

        console.log("getTradersWantedCards")
        console.log(user_id, trader_id)

        wantedCardModel.aggregate(
            // Pipeline
            [
                // Stage 1
                {
                    $match: {
                        user_id: mongoose.Types.ObjectId(trader_id)
                    }
                },

                // Stage 2
                {
                    $lookup: {
                        "from": "cardsInfo",
                        "localField": "name",
                        "foreignField": "name",
                        "as": "cardInfo"
                    }
                },

                // Stage 3
                {
                    $project: {
                        name: "$name",
                        nonfoilQuantity: "$nonfoilQuantity",
                        foilQuantity: "$foilQuantity",
                        wanted_ids: {
                            $map: {
                                input: "$cardInfo",
                                as: "card",
                                in: "$$card._id"
                            }
                        }
                    }
                },

                // Stage 4
                {
                    $lookup: {
                        "from": "usercards",
                        "let": {wanted_ids: "$wanted_ids"},
                        "pipeline": [
                            {
                                $match:
                                    {
                                        $expr:
                                            {
                                                $and:
                                                    [
                                                        {$in: ["$card_id", "$$wanted_ids"]},
                                                        {$eq: ["$user_id", mongoose.Types.ObjectId(user_id)]},
                                                        {$eq: ["$type", "cards"]}
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as": "foundCard"
                    }
                },

                // Stage 5
                {
                    $unwind: {
                        path: "$foundCard"
                    }
                },

                // Stage 6
                {
                    $group: {
                        _id: "$name",
                        name : {$first: "$name"},
                        traderNonfoilQuantity: {$sum: "$foundCard.nonfoilQuantity"},
                        traderFoilQuantity: {$sum: "$foundCard.foilQuantity"},
                        nonfoilQuantity: {$first: "$nonfoilQuantity"},
                        foilQuantity: {$first: "$foilQuantity"},
                    }
                },
            ],
            (err, tradersCardsGeneral) => {
                if (err) {
                    console.log(err)
                }
                callback(tradersCardsGeneral)
            }
        );
    }

    getWantedCards({user_id : user_id, trader_id : trader_id }, (myWantedGeneral)=>{
        console.log(myWantedGeneral)
        getTradersWantedCards({user_id : user_id, trader_id : trader_id}, (tradersWantedGeneral)=>{
            console.log(tradersWantedGeneral)
            callback("done", {myWantedGeneral : myWantedGeneral, tradersWantedGeneral : tradersWantedGeneral})
        })
    })
}

const getNewChatMessages = function({user_id, trader_id, latestCreatedAt}, callback){

    console.log("getNewChatMessages")
    console.log({user_id, trader_id, latestCreatedAt})
    ChatMessageModel.find({$or : [ {sender_id : user_id, receiver_id : trader_id}, {receiver_id : user_id, sender_id : trader_id}], createdAt : {$gt : latestCreatedAt}}, null, {sort : {createdAt: 1}}, (err, chatMessages)=>{
        if(err){
            console.log(err)
            return callback("error")

        }
        // return callback(this.responses.done, chatMessages)
        return callback("done", chatMessages)


    })
}

const getAllInfo = function({user_id, trader_id, latestCreatedAt}, callback){

    console.log("getAllInfo")
    console.log(this)

    getNewChatMessages({user_id, trader_id, latestCreatedAt}, (error, chats)=>{
        getGeneralInCommonCards({user_id, trader_id}, (error, inCommonGeneral)=>{
            getInCommonCards({user_id, trader_id}, (error, inCommon)=>{
                getCartCards({user_id, trader_id}, (error, cardCards)=>{
                    return callback(this.responses.done, Object.assign({chats : chats}, {inCommonGeneral : inCommonGeneral},  {inCommon : inCommon}, {cartCards : cardCards} ))
                })
            })
        })
    })
}

const getCardsByPage = function({trader_id, count, page}, callback){

    console.log("getAllInfo")
    console.log(this)

    getNewChatMessages({user_id, trader_id, latestCreatedAt}, (error, chats)=>{
        getGeneralInCommonCards({user_id, trader_id}, (error, inCommonGeneral)=>{
            getInCommonCards({user_id, trader_id}, (error, inCommon)=>{
                getCartCards({user_id, trader_id}, (error, cardCards)=>{
                    return callback(this.responses.done, Object.assign({chats : chats}, {inCommonGeneral : inCommonGeneral},  {inCommon : inCommon}, {cartCards : cardCards} ))
                })
            })
        })
    })
}

const getCartCards = ({user_id, trader_id}, callback)=>{
    console.log("getCartCards")
    const cartCardQuery = {$or : [{wanter_id : user_id, trader_id : trader_id}, {wanter_id : trader_id , trader_id : user_id}]}

    // cartCardModel.find(cartCardQuery, (err, cart)=>{
    //     if(err) {console.log(err)}
    //     callback("done", cart)
    // })

    cartCardModel.aggregate(

        // Pipeline
        [
            // Stage 1
            {
                $match: {
                    $or : [{wanter_id : mongoose.Types.ObjectId(user_id), trader_id : mongoose.Types.ObjectId(trader_id)}, {wanter_id : mongoose.Types.ObjectId(trader_id) , trader_id : mongoose.Types.ObjectId(user_id)}]
                }
            },

            // Stage 2
            {
                $lookup: {
                    "from" : "cardsInfo",
                    "localField" : "card_id",
                    "foreignField" : "_id",
                    "as" : "cardInfo"
                }
            },

            // Stage 3
            {
                $unwind: {
                    path : "$cardInfo"
                }
            },

            // Stage 4
            {
                $project: {
                    card_id : 1,
                    trader_id : 1,
                    wanter_id : 1,
                    foilMyPrice : 1,
                    foilPriceType : 1,
                    foilQuantity :1 ,
                    nonfoilMyPrice : 1,
                    nonfoilPriceType : 1,
                    nonfoilQuantity : 1,
                    usd : "$cardInfo.usd",
                    eur : "$cardInfo.eur",
                    name : "$cardInfo.name",
                    set_code : "$cardInfo.set_code",
                    set_name : "$cardInfo.set_name"

                }
            },
        ],


       (err, cartCards)=>{
            if(err) {console.log(err)}
            callback("done", cartCards)
        }

    );


}

const getCloseTradersWithQuantities = ({user_id, distance, longitude, latitude}, callback)=>{
    console.log("getCloseTraders")

    console.log(user_id, distance, longitude, latitude)

    UserSchema.aggregate(

            // Pipeline
            [
                // Stage 1
                {
                    $geoNear: { near: { type: 'Point', coordinates: [ longitude, latitude ] },
                        distanceField: 'dist.calculated',
                        maxDistance: distance * 1609,
                        query: { _id: { '$ne': mongoose.Types.ObjectId(user_id) }, openForTrading : true },
                        includeLocs: 'dist.location',
                        spherical: true
                    }
                },

                // Stage 2
                {
                    $project: {
                        _id : 1
                    }
                },

                // Stage 3
                {
                    $lookup: {
                        "from" : "usercards",
                        "let" : {user_id : "$_id"},
                        "pipeline" : [
                            {$match :
                                    { $expr:
                                            { $and:
                                                    [
                                                        { $eq: [ "$type",  "cards" ] },
                                                        { $eq: [ "$user_id",  "$$user_id" ] }
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as" : "tradeCard"
                    }
                },

                // Stage 4
                {
                    $unwind: {
                        path : "$tradeCard",
                        preserveNullAndEmptyArrays : true // optional

                    }
                },

                // Stage 5
                {
                    $lookup: {
                        "from" : "usercards",
                        "let" : {card_id : "$tradeCard.card_id"},
                        "pipeline" : [
                            {$match :
                                    { $expr:
                                            { $and:
                                                    [
                                                        { $eq: [ "$type",  "wanted" ] },
                                                        { $eq: [ "$user_id",  mongoose.Types.ObjectId(user_id) ] },
                                                        { $eq: [ "$card_id",  "$$card_id" ] }
                                                    ]
                                            }
                                    }
                            }
                        ],
                        "as" : "wantedCard"
                    }
                },

                // Stage 6
                {
                    $unwind: {
                        path : "$wantedCard",
                        preserveNullAndEmptyArrays : true // optional
                    }
                },

                // Stage 7
                {
                    $project: {
                        foilQuantity : {
                            $cond : [ {$lte : [ "$wantedCard.foilQuantity", "$tradeCard.foilQuantity"]} , {$ifNull : ["$wantedCard.foilQuantity", 0]} , "$tradeCard.foilQuantity"]
                        },
                        nonfoilQuantity : {
                            $cond : [ {$lte : [ "$wantedCard.nonfoilQuantity", "$tradeCard.nonfoilQuantity"]} , {$ifNull : ["$wantedCard.nonfoilQuantity", 0]} , "$tradeCard.nonfoilQuantity"]
                        },


                    }
                },

                // Stage 8
                {
                    $group: {
                        _id : "$_id",
                        foilQuantity: {$sum : "$foilQuantity"},
                        nonfoilQuantity: {$sum : "$nonfoilQuantity"},
                    }
                },

                // Stage 9
                {
                    $lookup: {
                        "from" : "users",
                        "localField" : "_id",
                        "foreignField" : "_id",
                        "as" : "userInfo"
                    }
                },

                // Stage 10
                {
                    $unwind: {
                        path : "$userInfo"
                    }
                },

                // Stage 11
                {
                    $project: {
                        name : "$userInfo.name",
                        foilQuantity : "$foilQuantity",
                        nonfoilQuantity : "$nonfoilQuantity",
                        longitude : { $arrayElemAt: [ "$userInfo.location.coordinates", 0 ] },
                        latitude : { $arrayElemAt: [ "$userInfo.location.coordinates", 1 ] }

                    }
                },
            ],


    (err, tradersInfo)=>{
                if(err){console.log(err)}

                console.log(tradersInfo)
                callback("done", tradersInfo);

            });

    //
    // const query = {
    //     _id : {$ne :  user_id},
    //     location : {
    //         $near : {
    //             $geometry : {
    //                 type : "Point",
    //                 coordinates : [longitude, latitude],
    //                 $maxDistance: distance * 1609,
    //                 $minDistance: 0
    //             }
    //         }
    //     }
    // }
    //
    // console.log(query)
    //
    // UserSchema.find(
    //     query,
    //     {lean:true}, (err, data)=>{
    //         if(err){console.log(err)}
    //         // console.log(data)
    //         console.log(data)
    //     })

}

const getFoundUserCardsByToken = function({trader_id, token}, callback){


    console.log("getFoundUserCardsByToken")

    console.log({trader_id, token})
    var regex = new RegExp(`.*${token}.*`, "i");
    console.log(regex)

    console.log(this)
    userCardSchema.aggregate(

        // Pipeline
        [
            // Stage 1
            {
                $match: {
                    user_id : mongoose.Types.ObjectId(trader_id),
                    type : "cards",
                }
            },

            // Stage 2
            {
                $lookup: {
                    "from" : "cardsInfo",
                    "localField" : "card_id",
                    "foreignField" : "_id",
                    "as" : "cardInfo"
                }
            },

            // Stage 3
            {
                $unwind: {
                    path : "$cardInfo"
                }
            },

            // Stage 4
            {
                $match: {
                    "cardInfo.name" : regex
                }
            },
            {
                $project : {
                    _id : "$card_id",
                    foilQuantity : 1,
                    nonfoilQuantity : 1,
                    usd : "$cardInfo.usd",
                    eur : "$cardInfo.eur",
                }

            }
        ],

        (err, cardsFound)=>{
            if(err){console.log(err)}
            callback(this.responses.done, cardsFound)
        }
    );
}

const getTraderCardsPagination = function({trader_id, page, division}, callback){

    console.log("getTraderCardsPagination")

    console.log(trader_id, page, division)

    userCardSchema.aggregate(
        [
            {
                $match: {
                    user_id : mongoose.Types.ObjectId(trader_id),
                    type : "cards",
                }
            },
            {
                $lookup: {
                    "from" : "cardsInfo",
                    "localField" : "card_id",
                    "foreignField" : "_id",
                    "as" : "cardInfo"
                }
            },
            {
                $unwind: {
                    path : "$cardInfo"
                }
            },
            {
                $sort: {
                    "cardInfo.name" : 1
                }
            },
            {
                $skip: page * division
            },

            {
                $limit: division
            },
            {
                $project: {
                    card_id : "$card_id",
                    foilQuantity : 1,
                    nonfoilQuantity : 1,
                    usd : "$cardInfo.usd",
                    eur : "$cardInfo.eur"
                }
            },
        ],
        (err, cardsFound)=>{
            if(err){console.log(err)}

            if(cardsFound.length < 10){
                callback(this.responses.endOfList, cardsFound)

            }else{
                callback(this.responses.found, cardsFound)
            }
        }
    );
}

module.exports = {
    getCloserTraders,
    getInCommonQuantity,
    getInCommonCards,
    getGeneralInCommonCards,
    getAllInfo,
    getTraderCardsPagination,
    getFoundUserCardsByToken,
    getCloserTradersAndOpenCarts,
    getCloserTradersWithInfo
}