const mongoose = require("mongoose")
const chatMessageModelName = require("../mongodb/Models/ChatMessage").model
const ChatMessageModel = mongoose.model(chatMessageModelName)

const sendChatMessage = function({sender_id, receiver_id, content, UUID}, callback){
    const chatMessage = new ChatMessageModel({content : content, receiver_id : receiver_id, sender_id : sender_id, UUID : UUID, state : 1})
    chatMessage.save((err, data)=>{
        if(err){console.log(err)}
        callback("serverReceived", data)
    })
}


const getNewChatMessages = function({user_id, trader_id, latestCreatedAt}, callback){

    console.log("getNewChatMessages")
    console.log({user_id, trader_id, latestCreatedAt})
    ChatMessageModel.find({$or : [ {sender_id : user_id, receiver_id : trader_id}, {receiver_id : user_id, sender_id : trader_id}], createdAt : {$gt : latestCreatedAt}}, null, {sort : {createdAt: 1}}, (err, chatMessages)=>{
        if(err){
            console.log(err)
            return callback("error")

        }
        return callback(this.responses.done, chatMessages)


    })
}

module.exports = {
    sendChatMessage,
    getNewChatMessages

}