const mongoose = require("mongoose")
var request = require("request");

const userModelName = require("../mongodb/Models/User").model
const UserModel = mongoose.model(userModelName)

const jwt = require("jsonwebtoken")
const bcryptjs = require("bcryptjs");
const fse = require("fs-extra");
const Handlebars = require("handlebars");
const fs = require('fs-extra')

const googleValues = JSON.parse(fs.readFileSync("Keys/googleAuth.json", "utf8"))
const {OAuth2Client} = require('google-auth-library')
const googleClientId = googleValues.web.client_id
const client = new OAuth2Client(googleClientId);

const googleLoginPost = function({googleIdToken}, callback){
    console.log("facebookLoginPOST");

    async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: googleIdToken,
            audience: googleClientId,  // Specify the CLIENT_ID of the app that accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        });
        const payload = ticket.getPayload();

        console.log(payload)
        const userid = payload['sub'];
        // If request specified a G Suite domain:
        //const domain = payload['hd'];

        console.log(userid)

        UserModel.findOne({googleIdToken : payload.sub}, (err, foundUser)=>{
            if(err){console.log(err)}

            if(foundUser){
                console.log("found user")
                callback("foundUser", foundUser._id)

            }else{
                UserModel.findOne({email : payload.email}, (err, foundUser)=>{
                    if(err){console.log(err)}
                    if(foundUser){
                        foundUser.set({googleUserId : payload.id})
                        foundUser.save((err, newUser)=>{
                            if(err){console.log(err)}
                            callback("synced", newUser._id)
                        })
                    }else{
                        var tempUser = new UserModel({email : payload.email, name : payload.given_name, googleUserID : payload.sub})
                        tempUser.save((err, newUser)=>{
                            if(err){console.log(err)}
                            callback("created", newUser._id)
                        })
                    }
                })
            }
        })
    }
    verify().catch(console.error);


}

module.exports = {
    googleLoginPost
}