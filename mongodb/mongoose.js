const mongoose = require("mongoose")

module.exports = ()=> {

    require("./Models/CardsInfo").schema(mongoose)
    require("./Models/ChatRoom").schema(mongoose)
    require("./Models/ChatMessage").schema(mongoose)
    require("./Models/TradeRoom").schema(mongoose)
    require("./Models/User").schema(mongoose)
    require("./Models/UserCard").schema(mongoose)
    require("./Models/EmailOneTimeToken").schema(mongoose)
    require("./Models/FCMToken").schema(mongoose)
    require("./Models/CartCard").schema(mongoose)
    require("./Models/WantedCard").schema(mongoose)

    mongoose.connect("mongodb://localhost/TCGBinder", {useNewUrlParser: true})

    return mongoose
}
