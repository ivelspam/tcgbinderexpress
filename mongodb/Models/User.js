const model = "Users"


module.exports = {model : model , schema : (mongoose)=>{

    const schema = new mongoose.Schema({
        name : {
            type : String,
            Required : true
        },
        accountStatus : {
            type : Number,
            default : 1,
            required : true
        },
        openForTrading : {
            type : Boolean,
            required : true,
            default : true
        },
        email : {
            type : String,
            required : true,
            createIndex : true,
            unique: true
        },
        tradeInfo : {
                type : String,
                default : ""
            },
            imageVersion : {
            type : Number,
            default : 0,
            required : false
        },
        measurementSystem : {type : String, default : "mi"},

        distance : {type : Number, default : 20},

        facebookUserId : {
            type : String,
            required : false,
            createIndex : {
                unique : true,
                partialFilterExpression : {FacebookUserId: {$type : String}}
            },
        },
        googleUserId : {
            type : String,
            required : false,
            createIndex : {
                unique : true,
                partialFilterExpression : {FacebookUserId: {$type : String}}
            },
        },
        emailConfirmed : {type : Boolean, default : false},
        password : {type : String, required : false, minLength: 8, maxLength: 32},
        location : {  type : {type : String, default : "Point"}, coordinates : {type : [Number], default : [0, 0]}},
        sharingAllCards : {type : Boolean, default : false},
        binders : {
                type : [{
                name : {type : String, required : true, minLength : 1, maxLength : 16, trim: true},
                alwaysOpen : {
                    type : Boolean,
                    default : false,

                }
            }],
            default : []
        }
    },
        {timestamps : true}
    )


    schema.path('password').validate((value)=>{
        return !value.match(/^[a-zA-Z0-9]+$/)
    }, "Password contains space");

    schema.index({location : "2dsphere"})

    var handleE11000 = function(error, res, next) {

        if (error.name === 'MongoError' && error.code === 11000) {
            next(new Error('There was a duplicate key error'))
        } else {
            next()
        }
    }

    schema.post('save', handleE11000)
    schema.post('update', handleE11000)
    schema.post('findOneAndUpdate', handleE11000)
    schema.post('insertMany', handleE11000)

    mongoose.model(model, schema)

}}