const model = "ChatMessage"

module.exports = {model : model , schema : (mongoose)=>{
    const schema = new mongoose.Schema({
        sender_id : { type : mongoose.Schema.ObjectId, ref : require("./User").model, required : true},
        receiver_id : {type : mongoose.Schema.ObjectId, ref : require("./User").model, required : true},
        content : { type : String },
        UUID : {type : String, required : true},
        state : {type : Number, default : 0}
    }, { timestamps : true })

    schema.index({sender_id : 1, receiver_id : 1})
    schema.index({receiver_id : 1, sender_id : 1})
    schema.index({sender_id : 1})
    schema.index({receiver_id : 1})
    schema.index({UUID : 1})

    mongoose.model(model, schema)
}}