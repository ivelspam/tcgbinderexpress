const model = "ChatRoom"

module.exports = {model : model , schema : (mongoose)=>{
    function arrayLimit(val){
        return val.length == 2
    }

    const schema = new mongoose.Schema({
        _id : {first : {type : mongoose.Schema.ObjectId}, second : {type : mongoose.Schema.ObjectId}}
    })
    mongoose.model(model, schema)
}}