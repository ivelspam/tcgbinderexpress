const model = "UserCard"

module.exports = {model : model , schema : (mongoose)=>{
        const schema = new mongoose.Schema({
                user_id : {type : mongoose.Schema.ObjectId, required : true},
                card_id : {type : String, required : true},
                type : {type : String, required : true},
                foilQuantity : {type : Number, required : true, default : 0},
                nonfoilQuantity : {type : Number, required : true, default : 0},
                binder_id : {type : String, default : ""}
            })

        schema.index({user_id : 1, card_id : 1, type : 1}, {unique : true})
        schema.index({user_id : 1})
        schema.index({card_id : 1})


        mongoose.model(model, schema)

}}