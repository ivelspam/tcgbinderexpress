const model = "cardsInfo"

module.exports = {model : model , schema : (mongoose)=>{

    const Schema = mongoose.Schema
    const schema = new Schema(
        {
            colors : [{type : String}],
            color_identity : [{type : String}],
            eur : {  type : Number, required : false},
            foil : {type : Boolean, required : true},
            _id : String,
            name : String,
            nonfoil : {type : Boolean, required : true},
            set_name : String,
            set_code : String,
            nonfoil_price : { type : Number, required : false},
            usd : {  type : Number, required : false},
            priceUpdatedAt : {type : Date, default : "1970-01-01T00:00:00.001+0000"}
        },
        {
            collection : "cardsInfo", timestamps : true
        }
    )

    schema.index({name : 1})

    var handleE11000 = function(error, res, next) {
        if (error.name === 'MongoError' && error.code === 11000) {
            next({error : error, value :  new Error('There was a duplicate key error')});
        } else {
            next();
        }
    };

    schema.post('save', handleE11000);
    schema.post('update', handleE11000);
    schema.post('findOneAndUpdate', handleE11000);
    schema.post('insertMany', handleE11000);

    mongoose.model(model, schema)
}}