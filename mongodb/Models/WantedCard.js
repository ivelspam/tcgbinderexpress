const model = "WantedCard"

module.exports = {model : model , schema : (mongoose)=>{
        const schema = new mongoose.Schema({
            user_id : {type : mongoose.Schema.ObjectId, required : true},
            name : {type : String, required : true},
            foilQuantity : {type : Number, required : true, default : 0},
            nonfoilQuantity : {type : Number, required : true, default : 0}
        })

        schema.index({user_id : 1, name : 1}, {unique : true})
        schema.index({name : 1})
        mongoose.model(model, schema)
    }}