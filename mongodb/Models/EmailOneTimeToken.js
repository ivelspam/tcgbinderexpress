const model = "EmailOneTimeToken"

module.exports = {model : model , schema : (mongoose)=>{
    const schema = new mongoose.Schema({
            token : {
              type : String,
              createIndex : true
            }
        },
        {timestamps : true})

    schema.index({token : 1})

    mongoose.model(model, schema)
}}