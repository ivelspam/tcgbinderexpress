const model = "FCMToken"

module.exports = {model : model , schema : (mongoose)=>{
    const schema = new mongoose.Schema({
                user_id : {
                    type : String,
                    ref : require("./User").model
                },
                FCMToken : {
                    type : String,
                    required : true
                },
                deviceType : String,
                token : {
                    type : String,
                    createIndex : true
                }
            },
            {timestamps : true}
    )

        schema.index({FMCToken : 1})
        schema.index({token : 1})
        schema.index({user_id : 1})
        schema.index({user_id : 1, FCMToken : 1, deviceType : 1})
        schema.index({deviceType : 1})

        mongoose.model(model, schema)
}}