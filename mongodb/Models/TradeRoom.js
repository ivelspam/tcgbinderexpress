const model = "TradeRoom"

module.exports = {model : model , schema : (mongoose)=>{

        var cardObject = {
                type :  [{
                _id : {type : String, required : true, ref : require("./CardsInfo").model},
                foil : {type : Number, required : true, default : 0},
                nonfoil : {type : Number, required : true, default : 0},
                binder : {type : String, required : false},
            }], default : []}

    const schema = new mongoose.Schema({
        state : Number,
        endedAt : Date,
        Creator : {
            type : mongoose.Schema.ObjectId,
            ref : require("./User").model,
            confirmed : Boolean,
            cards : cardObject
        },

        NotCreator : {
            type : mongoose.Schema.ObjectId,
            ref : require("./User").model,
            confirmed : Boolean,
            cards : cardObject
        }

    })

    schema.index({Creator : 1, NotCreator : 1,  })


        mongoose.model(model, schema)
}}