const model = "CartCard"

module.exports = {model : model , schema : (mongoose)=>{

        const schema = new mongoose.Schema({
            wanter_id : {type : mongoose.Types.ObjectId, required : true},
            trader_id : {type : mongoose.Types.ObjectId, required : true},
            card_id : {type : String, required : true},
            foilQuantity : {type : Number, default: 0},
            nonfoilQuantity : {type : Number, default: 0},
            foilPriceType : {type : Number, default: 1},
            nonfoilPriceType : {type : Number, default: 0},
            foilMyPrice : {type : Number, default: 0.0},
            nonfoilMyPrice : {type : Number, default: 0.0, required : true}

        })

        schema.index({wanter_id : 1, trader_id : 1, card_id : 1})
        schema.index({wanter_id : 1})
        schema.index({trader_id : 1})

        mongoose.model(model, schema)
}}