const jwt = require("jsonwebtoken");
const fs = require("fs-extra");
const usersConnected = {}
const ChatRoomAPI = require("./APIs/ChatRoomAPI");
const CartCardAPI = require("./APIs/CartCardsAPI");

const Constants = {
    sendChatMessage: "sendChatMessage",
    addWantCartCard: "addWantCartCard",
    updateCartCard: "updateCartCard",
    removeCartCard: "removeCartCard",
    clearCart: "clearCart",

    chatRoomId: (_id1, _id2)=>{
        // _id1 = JSON.stringify(_id1).replace("\"", "")
        // _id2 = JSON.stringify(_id2).replace("\"", "")

        _id1 = String(_id1)
        _id2 = String(_id2)

        if(_id1.toLowerCase().localeCompare(_id2.toLowerCase()) === -1){
            return `chat_${_id1}_${_id2}` ;
        }else{
            return `chat_${_id2}_${_id1}` ;
        }
    },
    traderRoomId: (_id1, _id2)=>{
        _id1 = String(_id1)
        _id2 = String(_id2)

        if(_id1.toLowerCase().localeCompare(_id2.toLowerCase()) === -1){
            return `trade_${_id1}_${_id2}`;
        }else{
            return `trade_${_id2}_${_id1}`;
        }
    },
    cartRoom: (_id1, _id2)=>{
        _id1 = String(_id1)
        _id2 = String(_id2)

        if(_id1.toLowerCase().localeCompare(_id2.toLowerCase()) === -1){
            return `cart_${_id1}_${_id2}`;
        }else{
            return `cart_${_id2}_${_id1}`;
        }
    }
}

module.exports = function(httpsServer){
    const socketio = require("socket.io")
    const io = socketio.listen(httpsServer);


    io.use((socket, next)=>{

        console.log("Trying to authenticating socket token")
        // console.log(socket.handshake)

        if (socket.handshake.query && socket.handshake.query.token){
            console.log("Token: " + socket.handshake.query.token);
            jwt.verify(socket.handshake.query.token,  fs.readFileSync("Keys/confirmed.key", "utf8"), function(err, decoded) {
                if(err) return next(new Error('Authentication error'));
                socket.decoded = decoded;
                next();
            });
        } else {
            console.log("Authentication error")
            next(new Error('Authentication error'));
        }
    }).on('connection', function (socket) {
        console.log("New Connection" )

        const values = { _id : socket.decoded._id, socket : socket, socketid : socket.id}

        if(usersConnected[values._id] != undefined){
            usersConnected[values._id].socket.disconnect
        }

        usersConnected[values._id] = values

        socket.on("disconnect", (data)=>{
            console.log("disconnecteeee")
            console.log(data)
            delete usersConnected[values._id]
        })

        socket.on("connect_failed", (err, data)=>{
            console.log("connect_failed")
            console.log(err)
            console.log(data)
        })

        socket.on(Constants.sendChatMessage, function (data, fn) {

            var message;
            if(typeof data == "object"){
                message = data
            }else{
                message = JSON.parse(data)
            }
            ChatRoomAPI.sendChatMessage({content : message.content, receiver_id : message.receiver_id, sender_id : message.sender_id,  UUID : message.UUID}, (response, newMessage)=>{


                console.log(response)
                fn(response, newMessage)
                if(response != null){
                    console.log(newMessage)
                    if(usersConnected[newMessage.receiver_id] != null){
                        io.sockets.connected[usersConnected[newMessage.receiver_id].socketid].emit(Constants.chatRoomId(newMessage.receiver_id, newMessage.sender_id), newMessage)
                    }
                }
            })
        });

        socket.on(Constants.addWantCartCard, function (data, fn) {
            console.log(Constants.addWantCartCard)

            let params;
            if(typeof data == "object"){
                params = data
            }else{
                params  = JSON.parse(data)
            }

            params.wanter_id = values._id

            console.log(params)

            CartCardAPI.addCartCardOrUpdate(params, (response, cartCard)=>{
                fn(response, cartCard)
                if(response != null){
                    if(usersConnected[params.trader_id] != null){
                        console.log(Constants.cartRoom(params.trader_id, params.wanter_id))
                        io.sockets.connected[usersConnected[params.trader_id].socketid].emit(Constants.cartRoom(params.trader_id, params.wanter_id), response, cartCard)
                    }
                }
            })
        });

        socket.on(Constants.updateCartCard, function (data, fn) {
            console.log(Constants.updateCartCard)

            var cartCard;
            if(typeof data == "object"){
                cartCard = data
            }else{
                cartCard  = JSON.parse(data)
            }

            let params = Object.assign({}, cartCard)
            params.user_id = values._id

            console.log(params)

            CartCardAPI.updateCartCard(params, (response, newMessage)=>{
                fn(response, newMessage)
                if(response != null){
                    console.log(newMessage)
                    if(usersConnected[params.trader_id] != null){
                        console.log(Constants.cartRoom(params.trader_id, params.user_id))
                        io.sockets.connected[usersConnected[params.trader_id].socketid].emit(Constants.cartRoom(params.trader_id, params.user_id), response, newMessage)
                    }
                }
            })
        });

        socket.on(Constants.removeCartCard, function (data, fn) {
            console.log(Constants.removeCartCard)

            var cartCard;
            if(typeof data == "object"){
                cartCard = data
            }else{
                cartCard  = JSON.parse(data)
            }

            let params = Object.assign({}, cartCard)
            params.user_id = values._id

            console.log(params)

            CartCardAPI.removeCartCard(params, (response, newMessage)=>{

                console.log("AAAAAAAAAAAAAAAAAAAAAAAA")
                console.log(response)
                console.log(newMessage)
                fn(response, newMessage)
                if(response != null){
                    console.log(newMessage)
                    if(usersConnected[params.trader_id] != null){
                        console.log(Constants.cartRoom(params.trader_id, params.user_id))
                        io.sockets.connected[usersConnected[params.trader_id].socketid].emit(Constants.cartRoom(params.trader_id, params.user_id), response, newMessage)
                    }
                }
            })
        });

        socket.on(Constants.clearCart, function (data, fn) {
            console.log(Constants.clearCart)

            var cartCard;
            if(typeof data == "object"){
                cartCard = data
            }else{
                cartCard  = JSON.parse(data)
            }

            let params = Object.assign({}, cartCard)
            params.user_id = values._id

            CartCardAPI.clearCart(params, (response)=>{
                fn(response)
                if(response != null){
                    if(usersConnected[params.trader_id] != null){
                        console.log(Constants.cartRoom(params.trader_id, params.user_id))
                        io.sockets.connected[usersConnected[params.trader_id].socketid].emit(Constants.cartRoom(params.trader_id, params.user_id), response)
                    }
                }
            })
        });

    });
}