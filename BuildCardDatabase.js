const fse = require("fs-extra")
const mongoose = require("mongoose")
var CardsInfoModel = mongoose.model(require("./mongodb/Models/CardsInfo").model)


var buildDatabaseFromFile = function(callback){
    var content = fse.readFileSync("./JSONs/scryfall-default-cards.json")
    var cards = JSON.parse(content);

        CardsInfoModel.find({}, (err, result)=>{

            console.log(result.length)

            if(result.length != cards.length){
                CardsInfoModel.deleteMany({}, ()=>{

                    const bulkData = []
                    for(var i = 0; i < cards.length; i++){
                        const row = cards[i]
                        const card = {}

                        console.log(i+1 + " of " + cards.length)

                        row.id != undefined ? card._id = row.id : null
                        row.name != undefined ? card.name = row.name : null
                        row.colors != undefined ? card.colors = row.colors : null
                        row.color_identity != undefined ? card.color_identity = row.color_identity : null
                        row.foil != undefined ? card.foil = row.foil : null
                        row.nonfoil != undefined ? card.nonfoil = row.nonfoil : null
                        row.set_name != undefined ? card.set_name = row.set_name : null
                        row.set != undefined ? card.set_code = row.set : null
                        row.usd != undefined ? card.usd = row.usd : null
                        row.eur != undefined ? card.eur = row.eur : null

                        bulkData.push(card)
                    }

                    CardsInfoModel.insertMany(bulkData)
                        .then(function(docs) {
                            console.log(`Done Creating Database. Number of Cards: ${docs.length}`)

                            callback();
                        })
                        .catch(function(err) {
                            console.log(err)
                        });
                })
            }else{
                console.log(`Database Allready Created. Number of Cards: ${cards.length}`)

                callback();

            }
        })
}






module.exports = {
    buildDatabaseFromFile
}


