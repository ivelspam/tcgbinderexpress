//server
const express = require('express')
const https = require("https")
const http = require("http")
const hbs = require("express-handlebars");
// const firebase = require('./Firebase/firebase')

//Final Value
const fs = require("fs-extra")

//
const bodyParser = require('body-parser')


//Mongodb
const mongoose = require("./mongodb/mongoose")()


//bottleneck
const bottleneck = require("./bottleneck");


//socketIO

const mainExpress = express()
mainExpress.use(bodyParser.urlencoded({ extended: true }))
mainExpress.use(bodyParser.json())
mainExpress.use(express.static("public"))

const router = express.Router({});

require("./Router/Router")(router)

mainExpress.use('/api',router)

mainExpress.engine("hbs", hbs({extname : "hbs", layoutsDir: __dirname + "/HTMLS"}))
    .set("view engine", "hbs");


const privateKey  = fs.readFileSync('ssl/privkey.pem', 'utf8')
const certificate = fs.readFileSync('ssl/fullchain.pem', 'utf8')

const credentials = {key: privateKey, cert: certificate}




require("./BuildCardDatabase").buildDatabaseFromFile(()=>{
    var httpServer = http.createServer(mainExpress).listen(80)
    const httpsServer = https.createServer(credentials, mainExpress).listen(443)

    const socketIOServer = https.createServer(credentials)

    socketIOServer.listen(3000)

    const socketIO = require("./SocketIO")(socketIOServer)

    // require("./bottleneck").downloadsSetsJSON();
    bottleneck.getAllThePrices();
})


