const CardAPI = require("../APIs/CardsInfoAPI")
const utils = require('../utils')
const links = require('../Constants').links
const mw = require("../middleware")

const getCardPrice = (router)=>{
    const link = links.cardsInfo_getPrice

    router.get(`/${link.name}`, link.checkSchema, mw.validator, function(req, res) {
        var query = req.query

        const calledFunction = CardAPI.getCardPrice.bind({responses : link.Response})

        calledFunction({card_id : query.card_id}, (message, price)=> {


            switch (message){
                case link.Response.found :
                    utils.makeJSONResponse(res, message, null, null, {price : price})
                    break
                case link.Response.notFound :
                    utils.makeJSONResponse(res, message)
                    break
            }
        })
    });
}

module.exports = function (router) {
    getCardPrice(router)
}