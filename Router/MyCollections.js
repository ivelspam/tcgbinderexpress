const UserCardsNdBindersAPI = require("../APIs/UserCardsAPI")
const mw = require("../middleware")
const utils = require('../utils')
const links = require('../Constants').links



const changeCardQuantity = (router)=>{
    const link = links.userCards_addUpdateCards
    router.post(`/${link.name}`, link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body

        const changeCardQuantity = UserCardsNdBindersAPI.changeCardQuantity.bind({responses : link.Response})

        changeCardQuantity({user_id : req.tokenData._id, card_id : body.card_id, foilQuantity : body.foilQuantity, nonfoilQuantity : body.nonfoilQuantity}, (message, data)=>{
            switch (message) {
                case link.Response.updated :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}

const getAllUserCards = (router)=>{
    const link = links.userCards_getAllUserCards
    router.get(`/${link.name}`, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        console.log(link.name)
        const body = req.body

        const getAllUserCards = UserCardsNdBindersAPI.getAllUserCards.bind({responses : link.Response})

        const params = {}
        params.user_id = req.tokenData._id

        getAllUserCards(params, (message, data)=>{
            switch (message) {
                case link.Response.gotAllCards :

                    console.log(data)
                    utils.makeJSONResponse(res, message, null, null, data)
                    break;
            }
        })
    })
}

const changeWantedQuantity = (router)=> {

    const link = links.userCards_addUpdateCardsToWanted

    router.post(`/${link.name}`, link.checkSchema, mw.validator,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body
        const changeWantedQuantity = UserCardsNdBindersAPI.changeWantedQuantity.bind({responses : link.Response})

        changeWantedQuantity({user_id : req.tokenData._id, card_id : body.card_id, foilQuantity : body.foilQuantity, nonfoilQuantity : body.nonfoilQuantity}, (message, data)=>{
            switch (message) {
                case link.Response.updatedWanted :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}

const deleteFromCards = (router)=> {

    const link = links.userCards_deleteCard

    router.post(`/${link.name}`, link.checkSchema, mw.validator,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function (req, res) {

        console.log(link.name)
        const body = req.body

        const deleteFromCards = UserCardsNdBindersAPI.deleteFromCards.bind({responses : link.Response})


        deleteFromCards({user_id: req.tokenData._id, card_id: body.card_id}, (message) => {
            switch (message) {
                case link.Response.deleted :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}

deleteWantedGeneralQuantity = (router)=>{

    const link = links.userCards_deleteWantedGeneralCard

    router.post(`/${link.name}`,   link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body
        const deleteWantedGeneralQuantity = UserCardsNdBindersAPI.deleteWantedGeneralQuantity.bind({responses : link.Response})

        deleteWantedGeneralQuantity({user_id : req.tokenData._id, name : body.card_id}, (message, data)=>{
            switch (message) {
                case link.Response.wantedGeneralDeleted :
                    utils.makeJSONResponse(res, message)
            }
        })
    })

}

const changeWantedGeneralQuantity = (router)=>{

    const link = links.userCards_addUpdateCardsToWantedGeneral

    router.post(`/${link.name}`, link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body
        const changeWantedGeneralQuantity = UserCardsNdBindersAPI.changeWantedGeneralQuantity.bind({responses : link.Response})

        changeWantedGeneralQuantity({user_id : req.tokenData._id, name : body.name, foilQuantity : body.foilQuantity, nonfoilQuantity : body.nonfoilQuantity}, (message, data)=>{
            switch (message) {
                case link.Response.updatedWantedGeneral :
                    utils.makeJSONResponse(res, message)
            }
        })
    })


}

const deleteWantedCard = (router)=>{

    const link = links.userCards_deleteWantedCard

    router.post(`/${link.name}`, link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        const body = req.body
        const deleteWantedCard = UserCardsNdBindersAPI.deleteWantedCard.bind({responses : link.Response})

        deleteWantedCard({user_id : req.tokenData._id, card_id : body.card_id}, (message, data)=>{
            switch (message) {
                case link.Response.wantedDeleted :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })

}


module.exports = function (router) {

    changeCardQuantity(router)
    changeWantedQuantity(router)
    deleteFromCards(router)
    deleteWantedGeneralQuantity(router)
    changeWantedGeneralQuantity(router)
    deleteWantedCard(router)
    getAllUserCards(router)

}