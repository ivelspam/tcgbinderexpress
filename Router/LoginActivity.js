const UserAPI = require("../APIs/UserAPI")
const FacebookAPI = require("../APIs/FacebookAPI")
const GoogleAPI = require("../APIs/GoogleAPI")
const mw = require("../middleware")
const utils = require('../utils')
const links = require('../Constants').links


const email_login = (router)=>{
    const link = links.email_login

    router.post(`/${link.name}` , link.checkSchema, mw.validator, function(req, res) {

        const body = req.body
        const emailLoginUser = UserAPI.emailLoginUser.bind({responses : link.Response});

        emailLoginUser({email : body.email, password : body.password}, (message, foundUser)=> {

            switch (message) {
                case link.Response.noMatch :
                    utils.makeJSONResponse(res, message)
                    break;
                case link.Response.match :
                    var keyValue = "Keys/confirmed.key";

                    if(foundUser.accountStatus == 0){
                        keyValue = "Keys/notConfirmed.key";
                    }

                    utils.makeJSONResponse(res, message, {
                        Email: foundUser.email,
                        _id: foundUser._id,
                        accountStatus: foundUser.accountStatus
                    }, keyValue)
                    break;
            }
        })
    });

}

const facebook_login = (router)=>{
    const link = links.facebook_login;

    router.post(`/${link.name}`, link.checkSchema, mw.validator, function(req, res) {

        console.log(link.name)
        const body = req.body
        const paramsNames = link.Params;

        const params = {}
        params[paramsNames.facebookUserId] = body[paramsNames.facebookUserId]
        params[paramsNames.facebookUserToken] = body[paramsNames.facebookUserToken]

        const facebookLoginPOST = FacebookAPI.facebookLoginPOST.bind({Responses : link.Response})

        facebookLoginPOST(params, (message, _id)=>{

            console.log(message)

            switch (message) {
                case link.Response.foundUser :
                case link.Response.created :
                case link.Response.synced :
                    var keyValue = "Keys/confirmed.key";
                    utils.makeJSONResponse(res, message, {
                        _id: _id,
                        accountStatus: 1
                    }, keyValue)
                    break;
            }
        })
    })

}

const google_login = (router)=>{
    const link = links.google_login;

    router.post(`/${link.name}`, function(req, res) {

        console.log(link.name)
        const body = req.body
        const paramsNames = link.Params;

        const params = {}

        console.log(body)
        params[paramsNames.googleIdToken] = body[paramsNames.googleIdToken]

        const googleLoginPost = GoogleAPI.googleLoginPost.bind({Responses : link.Response})

        googleLoginPost(params, (message, _id)=>{

            console.log(message)

            switch (message) {
                case link.Response.foundUser :
                case link.Response.created :
                case link.Response.synced :
                    var keyValue = "Keys/confirmed.key";

                    utils.makeJSONResponse(res, message, {
                        _id: _id,
                        accountStatus: 1
                    }, keyValue)
                    break;
            }
        })
    })

}

module.exports = function (router) {


    email_login(router)
    facebook_login(router)
    google_login(router)



}