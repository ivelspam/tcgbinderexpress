const UserAPI = require("../APIs/UserAPI")
const mw = require("../middleware")
const utils = require('../utils')
const links = require('../Constants').links


const changeOpenForTrading = (router)=>{

    const link = links.User_openForTrading

    router.post(`/${links.User_openForTrading.name}`,  link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body
        const paramsNames = link.Params

        const params = {}
        params.longitude = body[paramsNames.longitude]
        params.latitude = body[paramsNames.latitude]
        params.openForTrading = body[paramsNames.openForTrading]
        params.user_id = req.tokenData._id

        const changeOpenForTrading = UserAPI.changeOpenForTrading.bind({responses : link.Response})

        changeOpenForTrading(params, (message)=>{
            switch (message) {
                case link.Response.changed :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}


const updateTradeInfo = (router)=>{

    const link = links.user_updateTradeInfo

    router.post(`/${link.name}`,  link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {


        console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
        const body = req.body
        const paramsNames = link.Params

        const params = {}
        params.tradeInfo = body[paramsNames.tradeInfo]
        params.user_id = req.tokenData._id

        const updateTradeInfo = UserAPI.updateTradeInfo.bind({responses : link.Response})

        updateTradeInfo(params, (message)=>{
            switch (message) {
                case link.Response.updated :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}

const logOut = (router)=>{
    const link = links.logout_clear
    router.post(`/${link.name}`,  link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        const body = req.body
        const paramsNames = link.Params

        const params = {}
        params.user_id = req.tokenData._id

        const logoutPost = UserAPI.logOut.bind({responses : link.Response})

        logoutPost(params, (message)=>{
            switch (message) {
                case link.Response.loggedout :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}

const deleteAccount = (router)=>{

    const link = links.user_deleteAccount

    router.post(`/${link.name}`,  link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const params = {}
        params.user_id = req.tokenData._id

        const deleteAccount = UserAPI.deleteAccount.bind({responses : link.Response})

        deleteAccount(params, (message)=>{
            switch (message) {
                case link.Response.accountDeleted :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}


module.exports = function (router) {

    changeOpenForTrading(router)
    updateTradeInfo(router)
    logOut(router)
    deleteAccount(router)

}