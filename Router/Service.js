const mw = require("../middleware")
const TradersAPI = require("../APIs/TradersAPI")
const UserAPI = require("../APIs/UserAPI")

const utils = require('../utils')
const links = require('../Constants').links


const updateLocation = (router)=>{

    const link = links.user_updateLocation

    router.post(`/${link.name}`, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body
        const paramsNames = link.Params;

        const params = {}
        params[paramsNames.longitude] = body[paramsNames.longitude]
        params[paramsNames.latitude] = body[paramsNames.latitude]
        params.user_id = req.tokenData._id;

        console.log(params)

        const updateLocation = UserAPI.updateLocation.bind({responses : link.Response});

        updateLocation(params, (message, openCarts)=>{
            switch (message) {
                case link.Response.updated :
                case link.Response.error :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })
}


module.exports = function (router) {

    updateLocation(router)

}