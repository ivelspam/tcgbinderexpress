const links = require("../Constants").links
const UserAPI = require("../APIs/UserAPI")

const utils = require('../utils')
const JSONResponse = require("../JSONResponse");
const mw = require("../middleware")

module.exports = function (router) {

    require("./CardInfoRouter")( router)
    require("./EmailRegistrationRouter")( router)
    require("./LoginActivity")( router)
    require("./MyCollections")( router)
    require("./Service")( router)
    require("./TradeRouter")(router)
    require("./TradersListRouter")(router)
    require("./TradersListRouter")( router)
    require("./TransactionLoginScreen")(router)
    require("./ProfileRouter")(router)


    router.get('/', function(req, res) {
        console.log("GET /");
        JSONResponse(res, "hooray! welcome to our api! GET");
    });


    router.get('/', function(req, res) {
        console.log("GET /");
        JSONResponse(res, "hooray! welcome to our api! GET");
    });

    router.get(`/${links.email_confirmation.name}`, (req, res)=> {
        const token = req.query.token
        UserAPI.confirmEmailGET(token, (message)=>{
            switch (message) {
                case "tokenAlreadyUsed" :
                    utils.makeJSONResponse(res, message)
                    break;
                case "userNotFound" :
                    utils.makeJSONResponse(res, message)
                    break;
                case "emailConfirmed" :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        });
    });

    router.get('/privacy', function(req, res){
        res.render("privacy");
    });

    router.post('/', function(req, res) {
        res.json({ message: 'hooray! welcome to our api! POST' });
    });


    // return router
}