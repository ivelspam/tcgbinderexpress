const UserAPI = require("../APIs/UserAPI")
const FCMToken = require('../APIs/FCMToken')
const mw = require("../middleware")
const utils = require('../utils')
const links = require('../Constants').links




const getAllInfo = (router)=>{

    const link = links.allInfo_getAllInfo

    router.post(`/${links.allInfo_getAllInfo.name}`,  mw.printLink(links.allInfo_getAllInfo.name), mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        console.log(link.name);

        const body = req.body;

        UserAPI.getUserWithCardsPrices({_id : req.user._id, latitude : body[link.Params.latitude], longitude : body[link.Params.longitude]}, (message, userData)=>{

            switch (message){
                case links.allInfo_getAllInfo.Response.userFound :
                    utils.makeJSONResponse(res, message, null, null, { userData : userData})
                    break
                default :
            }
        })
    })
}

const updateFCMToken = (router, link)=>{

    router.post(`/${link.name}`, link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body;

        const params = {}
        params['user_id'] =  req.tokenData._id;
        params[link.Params.FCMToken] =  body.FCMToken;
        params[link.Params.deviceType] =  body.deviceType;

        const getNewChatMessages = FCMToken.updateFCMToken.bind({responses : link.Response})
        console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL")

        console.log(params)

        getNewChatMessages(params, (message, chatMessages)=>{
            switch (message){
                case link.Response.done :
                    utils.makeJSONResponse(res, message, null, null, { chatMessages : chatMessages})
                    break
                default :
                    utils.makeJSONResponse(res, "error")

            }
        })
    })
}


module.exports = function (router) {


    getAllInfo(router)
    updateFCMToken(router, links.FCMTOken_updateToken)
    //
    // router.post(`/${links.allInfo_getAllInfo.name}`,  mw.printLink(links.allInfo_getAllInfo.name), mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
    //
    //     const link = links.allInfo_getAllInfo
    //     console.log(link.name);
    //
    //     const body = req.body;
    //
    //
    //
    //     UserAPI.getUserWithCardsPrices({_id : req.user._id, latitude : body[link.Params.latitude], longitude : body[link.Params.longitude]}, (message, userData)=>{
    //
    //         switch (message){
    //             case links.allInfo_getAllInfo.Response.userFound :
    //                 utils.makeJSONResponse(res, message, null, null, { userData : userData})
    //                 break
    //             default :
    //         }
    //     })
    // })


}