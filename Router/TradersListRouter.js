const UserAPI = require("../APIs/UserAPI")
const TradersAPI = require("../APIs/TradersAPI")
const mw = require("../middleware")
const utils = require('../utils')
const links = require('../Constants').links

const CartCardsAPI = require("../APIs/CartCardsAPI")

const getTradersAndOpenCartsInfo = (router)=>{

    const link = links.traders_getCloserTradersAndOpenCarts

    router.get(`/${link.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const query = req.query
        const paramsNames = link.Params

        const params = {}
        params[paramsNames.latitude] =  parseFloat(query[paramsNames.latitude])
        params[paramsNames.longitude] =  parseFloat(query[paramsNames.longitude])
        params[paramsNames.distance] = parseInt(query[paramsNames.distance])
        params.user_id = req.tokenData._id

        const getTradersAndOpenCartsInfo = TradersAPI.getCloserTradersAndOpenCarts.bind({responses : link.Response})

        getTradersAndOpenCartsInfo(params, (message, data)=>{
            switch (message) {
                case link.Response.gotAllTradersInfo :
                    utils.makeJSONResponse(res, message, null, null, data)
                    break;
            }
        })
    })
}

const getCloserTradersWithInfoAndCart = (router)=>{

    const link = links.traders_getCloserTradersWithInfoAndCart

    router.get(`/${link.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        console.log(link.name)
        const query = req.query
        const paramsNames = link.Params
        console.log(query)

        console.log(paramsNames)

        const params = {}
        params[paramsNames.latitude] =  parseFloat(query[paramsNames.latitude])
        params[paramsNames.longitude] =  parseFloat(query[paramsNames.longitude])
        params[paramsNames.distance] = parseInt(query[paramsNames.distance])
        params.user_id = req.tokenData._id

        console.log(params)

        const getTradersAndOpenCartsInfo = TradersAPI.getCloserTradersWithInfo.bind({responses : link.Response})

        getTradersAndOpenCartsInfo(params, (message, data)=>{
            switch (message) {
                case link.Response.gotAllTradersInfo :
                    utils.makeJSONResponse(res, message, null, null, data)
                    break;
            }
        })
    })


}




module.exports = function (router) {

    getTradersAndOpenCartsInfo(router)
    getCloserTradersWithInfoAndCart(router)



    router.post(`/${links.User_UpdateDistanceAndMeasurementSystem.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        const link = links.User_UpdateDistanceAndMeasurementSystem;
        console.log(link.name)
        const body = req.body

        console.log(body)
        UserAPI.changeUserDistanceAndMeasurementType({user_id : req.tokenData._id, distance : body[link.Params.distance], measurementSystem : body[link.Params.measurementSystem]}, (message)=>{
            switch (message) {
                case link.Response.updated :
                    utils.makeJSONResponse(res, message)
                    break;
                case link.Response.notUpdated :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })


    router.post(`/${links.traders_getInCommonQuantity.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const link = links.traders_getInCommonQuantity;
        console.log(link.name)
        const body = req.body
        const params = {}
        params[link.Params.trader_id] = body[link.Params.trader_id]
        params.user_id = req.tokenData._id

        console.log(body)
        TradersAPI.getInCommonQuantity(params, (message, data)=>{
            switch (message) {
                case links.traders_getInCommonQuantity.Response.found :
                    utils.makeJSONResponse(res, message, null, null, {info : data})
                    break
                case links.traders_getInCommonQuantity.Response.notFound :
                    utils.makeJSONResponse(res, message)
                    break
            }
        })
    })

    router.post(`/${links.cart_getOpen.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const link = links.cart_getOpen;
        const body = req.body
        const paramsNames = link.Params;

        const params = {}
        params.user_id = req.tokenData._id;

        CartCardsAPI.getOpenCarts(params, (message, openCarts)=>{
            switch (message) {
                case "done" :
                    utils.makeJSONResponse(res, message, null, null, {openCarts : openCarts})
                    break;
            }
        })
    })

}