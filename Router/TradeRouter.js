const mw = require("../middleware")
const TradersAPI = require("../APIs/TradersAPI")
const utils = require('../utils')
const CartCardsAPI = require("../APIs/CartCardsAPI")
const links = require("../Constants").links
const ChatRoomAPI = require("../APIs/ChatRoomAPI")
const UserCardsAPI = require("../APIs/UserCardsAPI")

const getCartCards = (router)=>{

    const link = links.cart_getCards

    router.post(`/${link.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {
        console.log(link.name)

        const body = req.body

        const paramsNames = link.Params;
        const responses = link.Response;

        const params = {}
        params[paramsNames.trader_id] = body[paramsNames.trader_id]
        params.user_id = req.tokenData._id;

        console.log(params);

        CartCardsAPI.getCartCards(params, (message, cardsInCart)=>{
            switch (message) {
                case responses.done :
                    utils.makeJSONResponse(res, message, null, null, {cards : cardsInCart})
                    break;
            }
        })
    })


}


const getTradersCardsByToken = (router)=>{

    const link = links.userCards_getTraderCardsByToken

    router.get(`/${link.name}`, function(req, res) {

        console.log(link.name)

        const query = req.query

        console.log(query)

        const paramsNames = link.Params;

        const params = {}


        params[paramsNames.trader_id] = query[paramsNames.trader_id]
        params[paramsNames.token] = query[paramsNames.token]

        const getFoundUserCardsByToken =  TradersAPI.getFoundUserCardsByToken.bind({responses : link.Response})

        console.log(params)

        getFoundUserCardsByToken(params, (message, cardsFound)=>{
            switch (message) {
                case link.Response.done :
                    utils.makeJSONResponse(res, message, null, null, {cards : cardsFound})
                    break;
            }
        })
    })


}



const getNewChatMessages = (router)=>{

    const link = links.chatRoom_getNewFromUser

    router.post(`/${link.name}`, link.checkSchema, mw.validator, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const body = req.body;

        const params = {}
        params['user_id'] =  req.tokenData._id;
        params[link.Params.trader_id] =  body.trader_id;
        params[link.Params.latestCreatedAt] =  body.latestCreatedAt;

        const getNewChatMessages = ChatRoomAPI.getNewChatMessages.bind({responses : link.Response})

        getNewChatMessages(params, (message, chatMessages)=>{
            switch (message){
                case link.Response.done :
                    utils.makeJSONResponse(res, message, null, null, { chatMessages : chatMessages})
                    break
                default :
                    utils.makeJSONResponse(res, "error")

            }
        })
    })
}

const getAllInfo = (router)=>{

    const link = links.traders_getAllInfo

    router.post(`/${link.name}`, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        console.log(link.name);
        const body = req.body

        const paramsNames = link.Params;
        const responses = link.Response;

        const params = {}
        params[paramsNames.trader_id] = body[paramsNames.trader_id]
        params[paramsNames.latestCreatedAt] = body[paramsNames.latestCreatedAt]

        params.user_id = req.tokenData._id;

        console.log(params);

        console.log(link)

        const getAllInfo = TradersAPI.getAllInfo.bind({responses : link.Response})

        getAllInfo(params, (message, tradeInfo)=>{
            switch (message) {
                case responses.done :
                    utils.makeJSONResponse(res, message, null, null, tradeInfo)
                    break;
            }
        })
    })
}


const getCardsByPage = (router)=>{

    const link = links.traders_getCardsByPage

    router.get(`/${link.name}`,  link.checkSchema, mw.validator, function(req, res) {

        console.log(link.name);
        const query = req.query

        const paramsNames = link.Params;
        const responses = link.Response;

        const params = {}
        params[paramsNames.trader_id] = query[paramsNames.trader_id]
        params[paramsNames.division] = parseInt(query[paramsNames.division])
        params[paramsNames.page] = parseInt(query[paramsNames.page])

        console.log(query)

        const getCardsByPage = TradersAPI.getTraderCardsPagination.bind({responses : link.Response})

        getCardsByPage(params, (message, cards)=>{
            switch (message) {
                case responses.found :
                case responses.endOfList :
                    utils.makeJSONResponse(res, message, null, null,  { cards : cards})
                    break;
            }
        })
    })
}


module.exports = function (router) {

    getCartCards(router)
    getNewChatMessages(router)
    getAllInfo(router)
    getTradersCardsByToken(router)
    getCardsByPage(router)

    router.post(`/${links.traders_getInCommonCards.name}`, mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const link = links.traders_getInCommonCards;
        console.log(link.name);
        const body = req.body

        const paramsNames = link.Params;
        const responses = link.Response;

        const params = {}
        params[paramsNames.trader_id] = body[paramsNames.trader_id]
        params.user_id = req.tokenData._id;

        console.log(params);

        TradersAPI.getInCommonCards(params, (message, inCommonCards)=>{
            switch (message) {
                case responses.found :
                    utils.makeJSONResponse(res, message, null, null, inCommonCards)
                    break;
                case responses.notFound :
                    utils.makeJSONResponse(res, message)
                    break;
            }
        })
    })



    router.post(`/${links.traders_getWantedInCommonGeneral.name}`,  mw.ensureToken, mw.authTokenConfirmed, mw.checkIfUserExists, function(req, res) {

        const link = links.traders_getWantedInCommonGeneral;
        const body = req.body

        const paramsNames = link.Params;

        const params = {}

        console.log(body)

        console.log(body)

        params[paramsNames.trader_id] = body[paramsNames.trader_id]
        params.user_id = req.tokenData._id;

        TradersAPI.getGeneralInCommonCards(params, (message, cards)=>{
            switch (message) {
                case "done" :
                    utils.makeJSONResponse(res, message, null, null, {cards : cards})
                    break;
            }
        })
    })



}