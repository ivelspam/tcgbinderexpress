const mongoose = require("mongoose")
const tradeRoomModelName = require("./mongodb/Models/TradeRoom").model
const tradeRoomSchema = mongoose.model(tradeRoomModelName)
const { checkSchema, validationResult } = require('express-validator/check');

const links = {

    //ALLINFO
    allInfo_getAllInfo : {
        name: "allInfo_getAllInfo",
        Response : {
            userFound : "userFound",
            userNotFound : "userNotFound"
        },
        ResponseFields : {

        },
        Params: {
            latitude : "latitude",
            longitude : "longitude",

        }
    },

    allInfo_getUpdates : {
        name: "allInfo_getUpdates",
        Response : {
            match : "match",
            noMatch : "noMatch"
        },
        ResponseFields : {

        },
        Param : {

        }
    },

    cardsInfo_getPrice : {
        name: "cardsInfo_getPrice",
        Response : {
            found : "found",
            notFound : "notFound",

        },
        ResponseFields : {
            price : "price"
        },
        Params : {
            card_id : "card_id"
        },


        checkSchema : checkSchema({
            card_id : {
                exists : true,
            }
        })
    },

    //CARDCard
    cardCard_getCard : {
        name: "chatRoom_getNewFromUser",
        Response : {
            done : "done"
        },
        ResponseFields : {
            chatMessages : "chatMessages"
        },
        Params: {
            trader_id : "trader_id",
            latestCreatedAt : "latestCreatedAt",
        }
    },

    cart_getCards : {
        name: "cart_getCards",
        Response : {
            done : "done"
        },
        ResponseFields : {
            chatMessages : "found",
            noCart : "cart"
        },
        Params: {
            trader_id : "trader_id",
        },
        checkSchema : checkSchema({
            trader_id : {
                exists : true
            }
        })
    },
    cart_clear : {
        name: "cart_clear",
        Response : {
            cleared : "cleared"
        },
        ResponseFields : {

        },
        Params: {
            trader_id : "trader_id",
        },
        checkSchema : checkSchema({
            trader_id : {
                exists : true
            }
        })
    },
    cart_getOpen : {
        name: "cart_getOpen",
        Response : {

        },
        ResponseFields : {

        },
        Params : {
        }
    },

    //CHAT ROOM
    chatRoom_getNewFromUser : {
        name: "chatRoom_getNewFromUser",
        Response : {
            done : "done"
        },
        ResponseFields : {
            chatMessages : "chatMessages"
        },
        Params: {
            trader_id : "trader_id",
            latestCreatedAt : "latestCreatedAt",
        },
        checkSchema : checkSchema({
            trader_id : {
                exists : true
            },
            latestCreatedAt : {
                latestCreatedAt : true
            }
        })
    },

    email_addUser : {
        name: "email_addUser",
        Response : {
            registered : "registered",
            accountExists : "accountExists",
        },
        ResponseFields : {

        },
        Params : {
            name : "name",
            email : "email",
            password : "password"
        },
        validator : {

        }
    },
    email_confirmation : {
        name: "email_confirmation",
        Response : {
            userFound : "cardAdded"
        },
        ResponseFields : {

        },
        Params : {

        }
    },
    email_login : {
        name: "email_login",
        Response : {
            match : "match",
            noMatch : "noMatch"
        },
        ResponseFields : {

        },
        Params : {

        },
        checkSchema : checkSchema({
            email : {
                isEmail : true,
            }
        })
    },

    userCards_setAlwaysShare : {
        name: "userCards_setAlwaysShare",
        Response : {
            globalModified : "globalModified",
            binderModified : "binderModified"
        },
        ResponseFields : {

        },
        Params : {
            binder_id : "binder_id",
            valueBoolean : "valueBoolean"
        }
    },
    userCards_updateMeasurementAndDistance : {
        name: "userCards_updateMeasurementAndDistance",
        Response : {
            userFound : "changed"
        },
        ResponseFields : {

        },
        Params : {
            distance : "distance",
            measurementSystem : "measurementSystem"
        }
    },
    userCards_getAllUserCards : {
        name: "userCards_getAllUserCards",
        Response : {
            gotAllCards : "gotAllCards"
        },
        ResponseFields : {
            cardsSet : "cardsSet",
            cardsGeneral : "cardsGeneral"
        },
        Params : {

        }
    },
    userCards_addUpdateCards : {
        name: "userCards_addUpdateCards",
        Response : {
            updated : "updated"
        },
        ResponseFields : {

        },
        Params : {
            card_id : "card_id",
            foilQuantity : "foilQuantity",
            nonfoilQuantity : "nonfoilQuantity"
        },
        checkSchema : checkSchema({
            card_id : {
                exists : true
            },
            foilQuantity : {
                isNumeric : true
            },
            nonfoilQuantity : {
                isNumeric : true
            },
        })
    },
    userCards_addUpdateCardsToWanted : {
        name: "userCards_addUpdateCardsToWanted",
        Response : {
            updatedWanted : "updatedWanted"
        },
        ResponseFields : {

        },
        Params : {
            card_id : "card_id",
            foilQuantity : "foilQuantity",
            nonfoilQuantity : "nonfoilQuantity"
        },
        checkSchema : checkSchema({
            card_id : {
                exists : true
            },
            foilQuantity : {
                isNumeric : true
            },
            nonfoilQuantity : {
                isNumeric : true
            },
        })
    },
    userCards_deleteCard : {
        name: "userCards_deleteCard",
        Response : {
            deleted : "deleted"
        },
        ResponseFields : {

        },
        Params : {
            card_id : "card_id"
        },
        checkSchema : checkSchema({
            card_id : {
                exists : true
            }
        })
    },
    userCards_deleteWantedCard : {
        name: "userCards_deleteWantedCard",
        Response : {
            wantedDeleted : "wantedDeleted"
        },
        ResponseFields : {

        },
        Params : {
            card_id : "card_id"
        },
        checkSchema : checkSchema({
            card_id : {
                exists : true
            }
        })
    },
    userCards_deleteWantedGeneralCard : {
        name: "userCards_deleteWantedGeneralCard",
        Response : {
            wantedGeneralDeleted : "wantedGeneralDeleted"
        },
        ResponseFields : {

        },
        Params : {
            card_id : "card_id"
        },
        checkSchema : checkSchema({
            card_id : {
                exists : true
            }
        })
    },

    userCards_addUpdateCardsToWantedGeneral : {
        name: "userCards_addUpdateCardsToWantedGeneral",
        Response : {
            updatedWantedGeneral : "updatedWantedGeneral"
        },
        ResponseFields : {

        },
        Params : {
            name : "name",
            foilQuantity : "foilQuantity",
            nonfoilQuantity : "nonfoilQuantity"
        },
        checkSchema : checkSchema({
            name : {
                exists : true
            },
            foilQuantity : {
                isNumeric : true
            },
            nonfoilQuantity : {
                isNumeric : true
            },
        })
    },
    userCards_addBinder : {
        name: "userCards_addBinder",
        Response : {
            userFound : "cardAdded"
        },
        ResponseFields : {
            binders : ""
        },
        Params : {
            name : "name"
        }
    },
    userCards_getTraderCardsByToken : {
        name: "userCards_getTraderCardsByToken",
        Response : {
            done : "done"
        },
        ResponseFields : {
        },
        Params : {
            trader_id : "trader_id",
            token : "token"
        }
    },
    userCards_setCardToBinder : {
        name: "userCards_setCardToBinder",
        Response : {
            cardBinderChanged : "cardBinderChanged"
        },
        ResponseFields : {
            binders : ""
        },
        Params : {
            card_id : "card_id",
            binder_id : "binder_id"
        },

    },


    //FCMTOken
    FCMTOken_updateToken : {
        name: "FCMToken_updateToken",
        Response : {
            foundUser : "updated"
        },
        ResponseFields : {
        },
        Params : {
            FCMToken : 'FCMToken',
            deviceType : 'deviceType'

        },
        checkSchema : checkSchema({
            FCMToken : {
                isString : true,
            },
            deviceType : {
                isString : true,
            }
        })
    },

    facebook_login : {
        name: "facebook_login",
        Response : {
            foundUser : "foundUser",
            synced :  "synced",
            created : "created"

        },
        ResponseFields : {
        },
        Params : {
            facebookUserId : "facebookUserId",
            facebookUserToken : "facebookUserToken"
        },
        checkSchema : checkSchema({
            facebookUserId : {
                isString : true,
            },
            facebookUserToken : {
                isString : true,
            }
        })
    },

    google_login : {
        name: "google_login",
        Response : {
            foundUser : "foundUser",
            synced :  "synced",
            created : "created"

        },
        ResponseFields : {
        },
        Params : {
            googleIdToken : "googleIdToken"
        },
        checkSchema : checkSchema({
            facebookUserId : {
                isEmail : true,
            },
            facebookUserToken : {
                isEmail : true,
            }
        })
    },

    //USER
    User_UpdateDistanceAndMeasurementSystem : {
        name: "User_UpdateDistanceAndMeasurementSystem",
        Response : {
            updated : "updated",
            notUpdated : "notUpdated"

        },
        ResponseFields : {

        },
        Params : {
            distance: "distance",
            measurementSystem: "measurementSystem"
        }
    },
    user_deleteAccount : {
        name: "user_deleteAccount",
        Response : {
            accountDeleted : "accountDeleted"
        },
        ResponseFields : {

        },
        Params : {

        },
        checkSchema : checkSchema({

        })
    },
    user_updateLocation: {
        name: "user_updateLocation",
        Response : {
            updated : "updated",
            error : "error"
        },
        ResponseFields : {

        },
        Params : {
            longitude: "longitude",
            latitude: "latitude"
        },
        checkSchema : checkSchema({
            longitude : {
                isNumeric : true,
            },
            latitude : {
                isNumeric : true,
            }
        })
    },
    User_openForTrading : {
        name: "User_openForTrading",
        Response : {
            changed : "changed"
        },
        ResponseFields : {

        },
        Params : {
            openForTrading: "openForTrading",
            longitude: "longitude",
            latitude: "latitude"
        },
        checkSchema : checkSchema({
            longitude : {
                isNumeric : true,
            },
            latitude : {
                isNumeric : true,
            },
            openForTrading : {
                isBoolean : true
            }
        })
    },
    user_updateTradeInfo : {
        name: "user_updateTradeInfo",
        Response : {
            updated : "updated"
        },
        ResponseFields : {

        },
        Params : {
            tradeInfo: "tradeInfo"
        },
        checkSchema : checkSchema({
            tradeInfo : {
                exists : true,
            }
        })
    },

    logout_clear : {
        name: "logout_clear",
        Response : {
            loggedout : "loggedout"
        },
        ResponseFields : {
        },
        Params : {
            loggedout: "loggedout"
        },
        checkSchema : checkSchema({

        })
    },

    logout_deleteAccount : {
        name: "logout_deleteAccount",
        Response : {
            accountDeleted : "accountDeleted"
        },
        ResponseFields : {
        },
        Params : {

        },
        checkSchema : checkSchema({

        })
    },


    traders_getByDistance : {
        name: "traders_getByDistance",
        Response : {

        },
        ResponseFields : {

        },
        Params : {
            longitude : "longitude",
            latitude : "latitude",
            distance : "distance"
        }
    },
    traders_getWantedInCommonGeneral : {
        name: "traders_getWantedInCommonGeneral",
        Response : {
            done : "done"
        },
        ResponseFields : {
            cards : "cards"
        },
        Params : {
            trader_id : "trader_id"
        }
    },
    traders_getInCommonQuantity : {
        name: "traders_getInCommonQuantity",
        Response : {
            found : "found",
            notFound : "notFound",
        },
        ResponseFields : {
            foilQuantity : "foilQuantity",
            nonfoilQuantity : "nonfoilQuantity"
        },
        Params : {
            trader_id : "trader_id"
        }
    },
    traders_getCloserTradersAndOpenCarts : {
        name: "traders_getCloserTradersAndOpenCarts",
        Response : {
            gotAllTradersInfo : "gotAllTradersInfo"
        },
        ResponseFields : {
            closerTraders : "closerTraders",
            openCarts : "openCarts"
        },
        Params : {
            latitude : "latitude",
            longitude : "longitude",
            distance : "distance"
        }
    },
    traders_getCloserTradersWithInfoAndCart : {
        name: "traders_getCloserTradersWithInfoAndCart",
        Response : {
            gotAllTradersInfo : "gotAllTradersInfo"
        },
        ResponseFields : {
            closerTraders : "closerTraders",
            openCarts : "openCarts"
        },
        Params : {
            latitude : "latitude",
            longitude : "longitude",
            distance : "distance"
        }
    },
    traders_getCardsByPage : {
        name: "traders_getCardsByPage",
        Response : {
            found : "found",
            endOfList : "endOfList"

        },
        ResponseFields : {
            cards : "cards"
        },
        Params : {
            trader_id : "trader_id",
            division : "division",
            page : "page",
        },
        checkSchema : checkSchema({
            trader_id : {
                exists : true,
            },
            division : {
                isNumeric : true
            },
            page : {
                isNumeric : true
            },
        })
    },

    traders_getInCommonCards : {
        name: "traders_getInCommonCards",
        Response : {
            found : "found",
            notFound : "notFound"

        },
        ResponseFields : {
            wanted : "wanted",
            trade : "wanted",
        },
        Params : {
            trader_id : "trader_id"

        }
    },

    traders_getAllInfo : {
        name: "traders_getAllInfo",
        Response : {
            done : "done"
        },
        ResponseFields : {

        },
        Params : {
            trader_id : "trader_id",
            latestCreatedAt : "latestCreatedAt",
        }
    }
}


module.exports = {links}